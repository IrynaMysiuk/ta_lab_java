<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Gems</h2>
                <table border="1">
                    <tr bgcolor="#FF8C00">
                        <th>Name</th>
                        <th>Preciousness</th>
                        <th>Origin</th>
                        <th>Parameters</th>
                        <th>YearOfProduction</th>
                        <th>Value</th>
                        <th>Valuable</th>
                    </tr>
                    <xsl:for-each select="gems/gem">
                        <xsl:sort select="name"/>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="preciousness"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="paramerets"/></td>
                            <td><xsl:value-of select="yearOfProduction"/></td>
                            <td><xsl:value-of select="value"/></td>
                            <td><xsl:value-of select="valuable"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

