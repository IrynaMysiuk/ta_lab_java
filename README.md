OOP Task
====================
- - -
Project   'Flower Shop'

You can choose one of the products in my program:
====================
- - -
1)flowers,
2)flowerpots,
3)decors

Flowers include: 
====================
- - -
1)daisies,
2)roses,
3)lilies

Flowerpots include:
====================
- - -
1)lemons,
2)orchids,
3)violets

Decors include:
====================
- - -
1)tape,
2)table flowers,
3)wrap flowers

Every pizza has the following property:
====================
- - -
1)color,
2)sort,
3)price,
4)numbers
