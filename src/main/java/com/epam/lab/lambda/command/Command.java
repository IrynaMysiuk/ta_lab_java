package com.epam.lab.lambda.command;

public interface Command {
    void execute(String value);
}
