package com.epam.lab.lambda.command;

public class Constants {
    private Constants() {

    }

    public static final int START_LOOP = 0;
    public static final int FINISH_LOOP = 4;
}
