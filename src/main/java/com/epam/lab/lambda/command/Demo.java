package com.epam.lab.lambda.command;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static com.epam.lab.lambda.command.Constants.FINISH_LOOP;
import static com.epam.lab.lambda.command.Constants.START_LOOP;

public class Demo {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        int chooseOptions;
        Options options = new Options();
        List<Command> availableCommands = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        logger.info("Please add options: " +
                "\n1.Anonymous class\n2.Lambda\n3.Method references\n4.Command object");
        for (int i = START_LOOP; i <= FINISH_LOOP; i++) {
            try {
                chooseOptions = scanner.nextInt();
            } catch (InputMismatchException e) {
                logger.error("You need typing number between 1-4 number!" + e.getStackTrace());
                break;
            }
            options.chooseMethod(chooseOptions);
            options.output();
        }
    }
}
