package com.epam.lab.lambda.command;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Options {

    private static Logger logger = LogManager.getLogger(Application.class);
    private List<Command> availableCommands = new ArrayList<>();

    public void chooseMethod(int chooseOptions) {
        switch (chooseOptions) {
            case 1: {
                availableCommands.add(new Command() {
                    @Override
                    public void execute(String value) {
                        logger.info("Anonymous Class: " + value);
                    }
                });
                break;
            }
            case 2: {
                availableCommands.add(value -> {
                    logger.info("Lambda: " + value);
                });
                break;
            }
            case 3: {
                availableCommands.add(new Options()::doSomething);
                break;
            }
            case 4: {
                availableCommands.add(s -> logger.info("Command object: " + s));
                break;
            }
            default: {
                logger.error("You entered incorrect number!");
            }
        }
    }

    public void output() {
        availableCommands.forEach(s -> s.execute("abb"));
    }

    public void doSomething(String value) {
        logger.info("Method references: " + value);
    }
}
