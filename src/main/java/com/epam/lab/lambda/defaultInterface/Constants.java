package com.epam.lab.lambda.defaultInterface;

public class Constants {
    private Constants() {

    }

    final static int FIRST_VALUE = 10;
    final static int SECOND_VALUE = 50;
    final static int THIRD_VALUE = 30;
    public static final int FIRST_VALUE_FOR_AVERAGE = 10;
    public static final int SECOND_VALUE_FOR_AVERAGE = 11;
    public static final int THIRD_VALUE_FOR_AVERAGE = 12;
}
