package com.epam.lab.lambda.defaultInterface;

@FunctionalInterface
public interface MyLambda {
    int getValue(int a, int b, int c);
}
