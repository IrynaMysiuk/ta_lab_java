package com.epam.lab.lambda.defaultInterface;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.epam.lab.lambda.defaultInterface.Constants.*;

public class Demo {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        MyLambda maxValue = (a, b, c) -> (Stream.of(a, b, c).max(Integer::compare).get());
        logger.info(" " + maxValue.getValue(FIRST_VALUE, SECOND_VALUE, THIRD_VALUE));

        MyLambda averageValue = (a, b, c) -> (int) IntStream.of(a, b, c).average().getAsDouble();
        logger.info(" " + averageValue.getValue
                (FIRST_VALUE_FOR_AVERAGE, SECOND_VALUE_FOR_AVERAGE, THIRD_VALUE_FOR_AVERAGE));
    }
}
