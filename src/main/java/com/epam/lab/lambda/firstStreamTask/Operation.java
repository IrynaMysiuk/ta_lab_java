package com.epam.lab.lambda.firstStreamTask;

import java.util.Comparator;
import java.util.List;

public class Operation {

    private List<? extends Number> list;

    public Operation(List<? extends Number> list) {
        this.list = list;
    }

    public Object getMaxValue() {
        return list.stream().max(Comparator.comparing(Object::toString)).get();
    }

    public Object getMinValue() {
        return list.stream().min(Comparator.comparing(Object::toString)).get();
    }

    public double getSumReduceValue() {
        return list.stream().mapToDouble(Number::doubleValue).reduce(Double::sum).getAsDouble();
    }

    public double getAverageIntValue() {
        return list.stream().mapToDouble(Number::doubleValue).average().getAsDouble();
    }

    public double getSumValue() {
        return list.stream().mapToDouble(Number::doubleValue).sum();
    }
}
