package com.epam.lab.lambda.firstStreamTask;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.lambda.firstStreamTask.Constants.*;

public class Demo {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        RandomGenerator randomGenerator = new RandomGenerator();
        List<Integer> randomList = new ArrayList<>();
        Operation operation = new Operation(randomList);
        randomList.addAll(randomGenerator.getRandomIntList(MAX_VALUE, FROM_RANDOM_NUMBER, TO_RANDOM_NUMBER));
        randomGenerator.outputList(randomList);
        logger.info("Max value: " + operation.getMaxValue());
        logger.info("Min value: " + operation.getMinValue());
        logger.info("Sum value using reduce: " + operation.getSumReduceValue());
        logger.info("Average value: " + operation.getAverageIntValue());
        logger.info("Sum value: " + operation.getSumValue());

        List<Double> randomDoubleList = new ArrayList<>(randomGenerator
                .getRandomDoubleList(MAX_VALUE, FROM_RANDOM_NUMBER, TO_RANDOM_NUMBER));
        Operation secondOperation = new Operation(randomDoubleList);
        randomGenerator.outputList(randomDoubleList);
        logger.info("Max value: " + secondOperation.getMaxValue());
        logger.info("Min value: " + secondOperation.getMinValue());
        logger.info("Sum value using reduce: " + secondOperation.getSumReduceValue());
        logger.info("Average value: " + secondOperation.getAverageIntValue());
        logger.info("Sum value: " + secondOperation.getSumValue());
    }
}
