package com.epam.lab.lambda.firstStreamTask;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomGenerator {
    private Random random;

    public RandomGenerator() {
        random = new Random();
    }

    public List<Double> getRandomDoubleList(int max, int fromNumber, int toNumber) {
        return random.doubles(max, fromNumber, toNumber).boxed().collect(Collectors.toList());
    }

    public List<Integer> getRandomIntList(int max, int fromNumber, int toNumber) {
        return random.ints(max, fromNumber, toNumber).boxed().collect(Collectors.toList());
    }

    public void outputList(List<? extends Number> randomList) {
        randomList.forEach(System.out::println);
    }
}
