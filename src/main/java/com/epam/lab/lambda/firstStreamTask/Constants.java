package com.epam.lab.lambda.firstStreamTask;

public class Constants {
    private Constants() {

    }

    public static final int MAX_VALUE = 6;
    public static final int FROM_RANDOM_NUMBER = 4;
    public static final int TO_RANDOM_NUMBER = 50;
}
