package com.epam.lab.lambda.secondStreamTask;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Procedure procedure = new Procedure();
        logger.info("Please enter words or symbols...");
        List<String> listSting = new ArrayList<>();
        procedure.getInputValue();
        logger.info("\nEntered values: ");
        procedure.listString.forEach(s -> logger.info(s));
        logger.info("\nCount unique symbols: " + (procedure.getContUnique()));
        procedure.getUniqueSortedList();
        logger.info("\nSorted unique symbols: ");
        procedure.getUniqueSortedList().forEach(e -> logger.info(e));
        logger.info("\nCount words: ");
        procedure.getCountWord().forEach((key, value) -> logger.info(key + "= " + value + ", "));
        logger.info("\nNon upper case words: ");
        procedure.getNonUpperCaseWord();
    }
}
