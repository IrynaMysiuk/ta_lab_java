package com.epam.lab.lambda.secondStreamTask;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Procedure {
    private static Logger logger = LogManager.getLogger(Application.class);
    List<String> listString = new ArrayList<>();

    public void getInputValue() {
        String inputText;
        Scanner scanner = new Scanner(System.in);
        do {
            inputText = scanner.nextLine();
            listString.add(inputText);
        } while (!inputText.equals(""));
        listString = listString.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
    }

    public long getContUnique() {
        return listString.stream().distinct().count();
    }

    public List<String> getUniqueSortedList() {
        return listString.stream().distinct().sorted(String::compareToIgnoreCase).collect(Collectors.toList());
    }

    public Map<String, Integer> getCountWord() {
        return listString.stream()
                .collect(Collectors.toMap(k -> k, v -> 1, Integer::sum));
    }

    public void getNonUpperCaseWord() {
        listString.forEach(f -> logger.info(Arrays.stream(f.split(""))
                .map(String::toLowerCase)
                .filter(letter -> !letter.equals(" "))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))));
    }
}
