package com.epam.lab.io_nio;

public class Constants {
    final static int MAXCOUNT = 1000000;
    final static int RIGHTBORDERSYMBOL = 2;
    final static int LEFTBORDERSYMBOL = 1;
    final static String WRITETEXT = "Hello\\n";
    final static String ERRORTEXT = "Wrong path %s. Please try again!";
    public static final String PATH_TO_FILE = "C:\\";
    final static String PATH_TO_PROPERTIES_FILE = "src/main/resources/path.properties";

    private Constants() {
    }
}
