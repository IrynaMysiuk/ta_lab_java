package com.epam.lab.io_nio;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class SerializeDeserializeWorker implements Serializable {
    private static final long serialVersionUID = 1L;
    public String name;
    public String address;
    public transient int SSN;
    public int number;
    private static Logger logger;

    SerializeDeserializeWorker() {
        logger = LogManager.getLogger(Application.class.getName());
    }

    public void mailCheck() {
        logger.info("Mailing a check to " + name + " " + address);
    }

    public void serializeWorker(String filename) {
        SerializeDeserializeWorker e = new SerializeDeserializeWorker();
        e.name = "Iryna Mysiuk";
        e.address = "Ukraine, Lviv";
        e.SSN = 11122333;
        e.number = 101;
        try (FileOutputStream fileOut = new FileOutputStream(filename);
             ObjectOutputStream out = new ObjectOutputStream(fileOut);) {
            out.writeObject(e);
            out.close();
            fileOut.close();
            logger.info("Serialized data is saved in /files/worker.dat");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void deserializeWorker(String filename) {
        SerializeDeserializeWorker e = null;
        try (FileInputStream fileIn = new FileInputStream(filename);
             ObjectInputStream in = new ObjectInputStream(fileIn);) {
            e = (SerializeDeserializeWorker) in.readObject();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            logger.info("SerializeDeserializeWorker class not found");
            c.printStackTrace();
            return;
        }
        logger.info("Deserialized Employee...");
        logger.info("Name: " + e.name);
        logger.info("Address: " + e.address);
        logger.info("SSN: " + e.SSN);
        logger.info("Number: " + e.number);
    }
}
