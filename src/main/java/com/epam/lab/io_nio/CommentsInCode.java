package com.epam.lab.io_nio;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CommentsInCode {
    private String text;
    private int count;
    private int startMultiLineComment;
    private int startOneLineComment;
    private boolean startComment;
    static Logger logger;

    CommentsInCode() {
        count = 0;
        startMultiLineComment = 0;
        startOneLineComment = 0;
        startComment = false;
        logger = LogManager.getLogger(Application.class.getName());
    }

    private void searchOneLineComment(String s1, int i) {
        if (s1.charAt(i) == '/' && s1.charAt(i + Constants.LEFTBORDERSYMBOL) == '/' && s1.charAt(i +
                Constants.RIGHTBORDERSYMBOL) != '*') {
            startOneLineComment = i;
            startComment = true;
            count = 0;
        }
        if (startComment) {
            count++;
        }
        if (s1.charAt(i) == '\\' && s1.charAt(i + Constants.LEFTBORDERSYMBOL) == 'n' && startComment) {
            logger.info(s1.substring(startOneLineComment, startOneLineComment + count - Constants.LEFTBORDERSYMBOL));
            startComment = false;
        }
    }

    private void searchMultiLineComment(String s1, int i) {
        if (s1.charAt(i) == '/' && s1.charAt(i + Constants.LEFTBORDERSYMBOL) == '*') {
            startMultiLineComment = i;
        }
        if (s1.charAt(i) == '*' && s1.charAt(i + Constants.LEFTBORDERSYMBOL) == '/') {
            logger.info(s1.substring(startMultiLineComment, i + Constants.RIGHTBORDERSYMBOL));
        }
    }

    private void BufferedRead(String filename) throws IOException {
        String line;
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
        while ((line = bufferedReader.readLine()) != null) {
            text += line + '\\' + 'n';
        }
        bufferedReader.close();
    }

    public void findComment(String filename) throws IOException {
        BufferedRead(filename);
        for (int i = 0; i < text.length(); i++) {
            searchMultiLineComment(text, i);
            searchOneLineComment(text, i);
        }
    }
}
