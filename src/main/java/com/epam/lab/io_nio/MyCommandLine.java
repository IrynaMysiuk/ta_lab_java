package com.epam.lab.io_nio;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.util.Scanner;

import static com.epam.lab.io_nio.Constants.PATH_TO_FILE;

public class MyCommandLine {

    private static Path path;
    private static Logger logger;
    private String cmd;
    private Exceptions exception;

    MyCommandLine() {
        path = Paths.get(PATH_TO_FILE);
        logger = LogManager.getLogger(Application.class.getName());
        exception = new Exceptions("");
    }

    private static void outputInfo(Path entry, Path path) {
        try {
            logger.info(String.format(
                    "\tFile Name: %s; Modified file:%s; Readable:%s; Writable:%s; Executable:%s; Size:%s; ",
                    entry.toString(), Files.getLastModifiedTime(entry), Files.isReadable(path), Files.isWritable(path),
                    Files.isExecutable(path), (double) Files.size(entry) / (1024)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void traverseDir(Path path) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {
                if (entry.toFile().isDirectory()) {
                    outputInfo(entry, path);
                    traverseDir(entry);
                } else {
                    outputInfo(entry, path);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void Dir(Path path) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {
                if (entry.toFile().isDirectory())
                    outputInfo(entry, path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readCmd(Scanner scan) {
        try {
            exception.findEmptyText(cmd);
            checkCmd();
        } catch (Exceptions e) {
            logger.error(String.format(Constants.ERRORTEXT, e));
        } catch (NoSuchFileException e) {
            logger.error(String.format(Constants.ERRORTEXT, e));
        }

    }

    private void executeCmd(String before, String after) throws NoSuchFileException, Exceptions {
        if (before.contains("cd") && exception.checkPath(after)) {
            path = Paths.get(after);
        }
        if (before.contains("dir")) {
            traverseDir(path);
            Dir(path);
        }
    }

    private void checkCmd() throws NoSuchFileException, Exceptions {
        cmd = cmd + " ";
        int indexOfDash = cmd.indexOf(' ');
        String before = cmd.substring(0, indexOfDash);
        String after = cmd.substring(indexOfDash + Constants.LEFTBORDERSYMBOL);
        executeCmd(before, after.trim());
    }

    public void startReadCmd(MyCommandLine myCmd, Scanner scan) {
        logger.info("Parent of the target: " + path.getFileSystem());
        logger.info("Please enter a command \"cd\" or \"dir\"");
        logger.info("Please enter \"exit\" to leave at the command line ");
        logger.info(
                "Attention! Format the command \"cd D:\\\\music\" for OS Windows and \"cd home\\music\" for OS Linux ");
        while (true) {
            logger.info(String.format("%s>", path.toString()));
            cmd = scan.nextLine();
            if (cmd.equals("exit"))
                break;
            myCmd.readCmd(scan);
        }
    }
}
