package com.epam.lab.io_nio.client_server;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import static com.epam.lab.io_nio.client_server.Const.*;

public class Server {

    private static Logger logger = LogManager.getLogger(Application.class);

    @SuppressWarnings("unused")
    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        InetSocketAddress address = new InetSocketAddress(HOST, PORT);
        serverSocket.bind(address);
        serverSocket.configureBlocking(false);
        int ops = serverSocket.validOps();
        SelectionKey selectKy = serverSocket.register(selector, ops, null);
        boolean isClosed = false;
        do {
            logger.info("i'm a server and i'm waiting for new connection and buffer select...");
            selector.select();
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> counter = keys.iterator();
            while (counter.hasNext()) {
                SelectionKey myKey = counter.next();
                if (myKey.isAcceptable()) {
                    SocketChannel client = serverSocket.accept();
                    client.configureBlocking(false);
                    client.register(selector, SelectionKey.OP_READ);
                    logger.info("Connection Accepted: " + client.getLocalAddress() + "\n");
                    isClosed = false;
                } else if (myKey.isReadable()) {
                    SocketChannel otherClient = (SocketChannel) myKey.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(MAX_LENGTH);
                    otherClient.read(buffer);
                    String result = new String(buffer.array()).trim();
                    logger.info("Message received: " + result);
                    if (result.equals("Client#5")) {
                        otherClient.close();
                        logger.info("\nIt's time to close connection as we got last client name 'Client#5'");
                        isClosed = true;
                    }
                }
                counter.remove();
            }
        } while (!isClosed);
    }
}
