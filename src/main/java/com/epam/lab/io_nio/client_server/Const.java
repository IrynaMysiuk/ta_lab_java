package com.epam.lab.io_nio.client_server;

public class Const {
    private Const() {
    }

    public static final String HOST = "localhost";
    public static final int PORT = 1111;
    public static final int WAIT_TIME = 2000;
    public static final int MAX_LENGTH = 256;
}
