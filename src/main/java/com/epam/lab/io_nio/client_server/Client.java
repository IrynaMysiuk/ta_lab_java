package com.epam.lab.io_nio.client_server;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

import static com.epam.lab.io_nio.client_server.Const.*;

public class Client {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        InetSocketAddress address = new InetSocketAddress(HOST, PORT);
        SocketChannel socketClient = SocketChannel.open(address);
        logger.info("Connecting to Server on port " + PORT);
        ArrayList<String> client = new ArrayList<>();
        client.add("Client#1");
        client.add("Client#2");
        client.add("Client#3");
        client.add("Client#4");
        client.add("Client#5");

        for (String clientNumber : client) {

            byte[] message = clientNumber.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(message);
            socketClient.write(buffer);
            logger.info("sending: " + clientNumber);
            buffer.clear();
            Thread.sleep(WAIT_TIME);
        }
        socketClient.close();
    }
}

