package com.epam.lab.io_nio.inputStream;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.PushbackInputStream;

public class Demo {
    public static final String INPUT_DATA = "1##2#34###12";
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        byte[] ary;
        ary = INPUT_DATA.getBytes();
        ByteArrayInputStream array = new ByteArrayInputStream(ary);
        PushbackInputStream push = new PushbackInputStream(array);
        int firstCounter;
        while ((firstCounter = push.read()) != -1) {
            if (firstCounter == '#') {
                int secondCounter;
                if ((secondCounter = push.read()) == '#') {
                    logger.info("**");
                } else {
                    push.unread(secondCounter);
                    logger.info((char) firstCounter);
                }
            } else {
                logger.info((char) firstCounter);
            }
        }
    }
}
