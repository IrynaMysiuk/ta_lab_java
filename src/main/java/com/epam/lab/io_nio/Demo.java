package com.epam.lab.io_nio;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import static com.epam.lab.io_nio.Constants.PATH_TO_PROPERTIES_FILE;

public class Demo {
    static Logger logger = LogManager.getLogger(Application.class.getName());
    static String filePathSerialize;
    static String filePathFindComments;
    static String filePathFormattingText;

    private static void setReadProperties() {
        Properties aProp = new Properties();
        try {
            aProp.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        filePathSerialize = aProp.getProperty("PathSerializeDeserialize.value");
        filePathFindComments = aProp.getProperty("PathFindComments.value");
        filePathFormattingText = aProp.getProperty("PathFormattingText.value");
    }

    public static void main(String[] args) throws IOException {
        setReadProperties();
        logger.info("-------------TASK 2---------------");
        SerializeDeserializeWorker worker = new SerializeDeserializeWorker();
        worker.serializeWorker(filePathSerialize);
        worker.deserializeWorker(filePathSerialize);
        logger.info("-------------TASK 3---------------");
        CompareReadingAndWriting reader = new CompareReadingAndWriting();
        reader.comparePerformance();
        logger.info("-------------TASK 5 ---------------");
        CommentsInCode parse = new CommentsInCode();
        parse.findComment(filePathFindComments);
        logger.info("-------------TASK 6---------------");
        Scanner scan = new Scanner(System.in);
        MyCommandLine myCmd = new MyCommandLine();
        myCmd.startReadCmd(myCmd, scan);
    }
}
