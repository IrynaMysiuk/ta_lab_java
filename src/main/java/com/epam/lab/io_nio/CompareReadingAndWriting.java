package com.epam.lab.io_nio;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class CompareReadingAndWriting {
    private static Logger logger;

    CompareReadingAndWriting() {
        logger = LogManager.getLogger(Application.class.getName());
    }

    private void bufferedWrite(String fileName) {
        String str = Constants.WRITETEXT;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (int i = 0; i < Constants.MAXCOUNT; i++)
                writer.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void usualWrite(String filename) {
        String str = Constants.WRITETEXT;
        try (FileOutputStream outputStream = new FileOutputStream(filename)) {
            byte[] strToBytes = str.getBytes();
            for (int i = 0; i < Constants.MAXCOUNT; i++)
                outputStream.write(strToBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void BufferedRead(String filename) {
        @SuppressWarnings("unused")
        String line = "";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filename))) {
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void usualRead(String fileName) {
        try (FileReader fr = new FileReader(fileName)) {
            @SuppressWarnings("unused")
            int i;
            while ((i = fr.read()) != -1) {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getTimeBufferedWrite() {
        long start = System.currentTimeMillis();
        bufferedWrite("test1.txt");
        long stop = System.currentTimeMillis();
        return (int) (stop - start);
    }

    private int getTimeUsualWrite() {
        long start = System.currentTimeMillis();
        usualWrite("test2.txt");
        long stop = System.currentTimeMillis();
        return (int) (stop - start);
    }

    private int getTimeBufferedRead() {
        long start = System.currentTimeMillis();
        BufferedRead("test1.txt");
        long stop = System.currentTimeMillis();
        return (int) (stop - start);
    }

    private int getTimeUsualRead() {
        long start = System.currentTimeMillis();
        usualRead("test2.txt");
        long stop = System.currentTimeMillis();
        return (int) (stop - start);
    }

    public void comparePerformance() {
        logger.info("Buffered write:" + getTimeBufferedWrite());
        logger.info("Usual write:" + getTimeUsualWrite());
        logger.info("Buffered read:" + getTimeBufferedRead());
        logger.info("Usual read:" + getTimeUsualRead());
    }
}
