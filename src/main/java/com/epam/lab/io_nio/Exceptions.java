package com.epam.lab.io_nio;

import java.io.File;
import java.nio.file.NoSuchFileException;

public class Exceptions extends Throwable {
    private static final long serialVersionUID = 1L;

    Exceptions(String s) {
        super(s);
    }

    void findEmptyText(String name) throws Exceptions {
        if ("".equals(name)) {
            throw new Exceptions("Row is empty!");
        }
    }

    boolean checkPath(String path) throws NoSuchFileException {
        boolean checkingPath;
        File file = new File(path);
        if (!file.isDirectory()) {
            throw new NoSuchFileException("no files found");
        } else {
            checkingPath = true;
        }
        return checkingPath;
    }
}
