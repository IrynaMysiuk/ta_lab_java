package com.epam.lab.logger;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;

public class SMS {
    public static final String ACCOUNT_SID = "NJfe3d25c848d537acaas6zbdbf5d329c5";
    public static final String AUTH_TOKEN = "42c25dg253d25ds3dbv4245475d32s862";

    public static void main(String[] args) {
        send("FATAL in your code!!!");
    }

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380671412989"),
                        new PhoneNumber("+380974564375"), str).create();
        System.out.println(message.getSid());
    }

    @Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
    public final class SmsAppender extends AbstractAppender {
        protected SmsAppender(String name, Filter filter,
                              Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
            super(name, filter, layout, ignoreExceptions);
        }

        @Override
        public void append(LogEvent event) {
            try {
                send(new String(getLayout().toByteArray(event)));
            } catch (Exception ex) {
            }
        }

        @PluginFactory
        public SmsAppender createAppender(
                @PluginAttribute("name") String name,
                @PluginElement("Layout") Layout<? extends Serializable> layout,
                @PluginElement("Filter") final Filter filter,
                @PluginAttribute("otherAttribute") String otherAttribute) {
            if (name == null) {
                LOGGER.error("No name provided for MyCustomAppenderImpl");
                return null;
            }
            if (layout == null) {
                layout = PatternLayout.createDefaultLayout();
            }
            return new SmsAppender(name, filter, layout, true);
        }
    }
}

