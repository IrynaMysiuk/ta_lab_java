package com.epam.lab.json;

public class Constants {
    public static final String PATH_TO_JSON = "src/main/resources/json/gem.json";
    public static final String PATH_TO_JSON_SCHEMA = "/json/gem_schema.json";

    private Constants() {
    }
}
