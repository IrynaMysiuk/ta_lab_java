package com.epam.lab.json.models;

import java.util.List;

public class Gems {
    private List<Gem> gems;

    public List<Gem> getGems() {
        return gems;
    }

    public void setGems(List<Gem> gems) {
        this.gems = gems;
    }
}
