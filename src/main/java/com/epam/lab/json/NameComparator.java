package com.epam.lab.json;

import com.epam.lab.json.models.Gem;

import java.util.Comparator;

public class NameComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        return ((Gem) o1).getName().compareTo(((Gem) o2).getName());
    }
}

