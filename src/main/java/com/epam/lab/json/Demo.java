package com.epam.lab.json;

import com.epam.lab.json.models.Gem;
import com.epam.lab.json.models.Gems;
import com.epam.lab.logger.Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import static com.epam.lab.json.Constants.PATH_TO_JSON;
import static com.epam.lab.json.Constants.PATH_TO_JSON_SCHEMA;

public class Demo {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Gems gems = mapper.readValue(new File(PATH_TO_JSON), Gems.class);
        String prettyJson = mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(gems);
        logger.info(prettyJson);

        for (Gem gem : gems.getGems())
            validateGem(gem);

        logger.info("Sorted Gems:");
        List<Gem> sortedGems = gems.getGems();
        sortedGems.sort(new NameComparator());
        for (Gem gem : sortedGems)
            logger.info(mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(gem));
    }

    private static void validateGem(Gem gem) {
        InputStream is = Demo.class.getResourceAsStream(PATH_TO_JSON_SCHEMA);
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(is));
        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(new JSONObject(gem));
    }
}