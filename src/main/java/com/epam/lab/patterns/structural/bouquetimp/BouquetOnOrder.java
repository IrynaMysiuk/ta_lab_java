package com.epam.lab.patterns.structural.bouquetimp;

import com.epam.lab.patterns.structural.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class BouquetOnOrder implements Bouquet {

    final private double PRICE = 90;
    final private String BOUQUET_ON_ORDER = "Bouquet on order";
    private List<String> toppings;

    public BouquetOnOrder() {
        toppings = new LinkedList<>();
        toppings.add("Paket");
        toppings.add("Roses");
        toppings.add("Lilies");
        toppings.add("Daisies");
    }

    @Override
    public double getCost() {
        return PRICE;
    }

    @Override
    public String getName() {
        return BOUQUET_ON_ORDER;
    }

    @Override
    public List<String> getToppings() {
        return toppings;
    }
}
