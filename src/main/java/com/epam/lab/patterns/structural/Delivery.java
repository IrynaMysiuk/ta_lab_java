package com.epam.lab.patterns.structural;

public class Delivery extends BouquetDecorator {

    private final double ADDITIONAL_PRICE = 30;
    private final String TO_NAME = " + Delivery";

    public Delivery() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setToName(TO_NAME);
    }
}
