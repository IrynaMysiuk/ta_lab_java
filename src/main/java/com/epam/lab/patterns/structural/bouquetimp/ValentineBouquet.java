package com.epam.lab.patterns.structural.bouquetimp;

import com.epam.lab.patterns.structural.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class ValentineBouquet implements Bouquet {
    final private double PRICE_BOUQUET = 110;
    final private String NAME_BOUQUET = "Bouquet";
    private List<String> toppings;

    public ValentineBouquet() {
        toppings = new LinkedList<>();
        toppings.add("Roses");
        toppings.add("Lilies");
    }

    @Override
    public double getCost() {
        return PRICE_BOUQUET;
    }

    @Override
    public String getName() {
        return NAME_BOUQUET;
    }

    @Override
    public List<String> getToppings() {
        return toppings;
    }
}
