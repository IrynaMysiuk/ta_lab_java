package com.epam.lab.patterns.structural;

import java.util.List;

public interface Bouquet {

    double getCost();

    String getName();

    List<String> getToppings();
}
