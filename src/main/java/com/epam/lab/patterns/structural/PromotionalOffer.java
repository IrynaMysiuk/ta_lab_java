package com.epam.lab.patterns.structural;

public class PromotionalOffer extends BouquetDecorator {

    private final double DISCOUNT = 100;
    private final String TO_NAME = " + Promotional Offer";

    public PromotionalOffer() {
        setAdditionalPrice(-DISCOUNT);
        setToName(TO_NAME);
    }
}
