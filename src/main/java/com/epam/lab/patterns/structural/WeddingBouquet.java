package com.epam.lab.patterns.structural;

public class WeddingBouquet extends BouquetDecorator {

    private final double ADDITIONAL_PRICE = 5;
    private final String ADDITIONAL_COMPONENT = "added Wedding Bouquet";

    public WeddingBouquet() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }
}
