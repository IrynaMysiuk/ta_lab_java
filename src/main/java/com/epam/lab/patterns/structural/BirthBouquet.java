package com.epam.lab.patterns.structural;

public class BirthBouquet extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 20;
    private final String ADDITIONAL_COMPONENT = "added Birth Bouquet";

    public BirthBouquet() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }
}
