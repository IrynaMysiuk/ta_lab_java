package com.epam.lab.patterns.structural.bouquetimp;

import com.epam.lab.patterns.structural.Bouquet;

import java.util.LinkedList;
import java.util.List;

public class BouquetWithCatalog implements Bouquet {

    final private double PRICE_BOUQUET = 180;
    final private String BOUQUET_WITH_CATALOG = "Bouquet with catalog";
    private List<String> toppings;

    public BouquetWithCatalog() {
        toppings = new LinkedList<>();
        toppings.add("Chrysanthemums");
        toppings.add("White daisies");
        toppings.add("Fern");
        toppings.add("Decor");
    }

    @Override
    public double getCost() {
        return PRICE_BOUQUET;
    }

    @Override
    public String getName() {
        return BOUQUET_WITH_CATALOG;
    }

    @Override
    public List<String> getToppings() {
        return toppings;
    }
}
