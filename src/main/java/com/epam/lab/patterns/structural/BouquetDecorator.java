package com.epam.lab.patterns.structural;

import java.util.List;
import java.util.Optional;

public class BouquetDecorator implements Bouquet {

    private Optional<Bouquet> bouquet;
    private double additionalPrice;
    private String toName = "";
    private String additionalComponent;

    public void setBouquet(Bouquet outBouquet) {
        bouquet = Optional.ofNullable(outBouquet);
        if (additionalComponent != null) {
            bouquet.orElseThrow(IllegalArgumentException::new).getToppings().add(additionalComponent);
        }
    }

    protected void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setAdditionalComponent(String additionalComponent) {
        this.additionalComponent = additionalComponent;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    @Override
    public double getCost() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getCost() + additionalPrice;
    }

    @Override
    public String getName() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getName() + toName;
    }

    @Override
    public List<String> getToppings() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getToppings();
    }
}
