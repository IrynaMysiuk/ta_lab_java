package com.epam.lab.patterns.structural;

import com.epam.lab.patterns.structural.bouquetimp.ValentineBouquet;

import java.util.List;

public class Client {

    public static void main(String[] args) {
        Bouquet pepperoniBouquet = new ValentineBouquet();
        BouquetDecorator weddingBouquet = new WeddingBouquet();
        BouquetDecorator birthBouquet = new BirthBouquet();
        BouquetDecorator birthBouquet1 = new BirthBouquet();
        BouquetDecorator delivery = new Delivery();
        BouquetDecorator discount = new PromotionalOffer();

        weddingBouquet.setBouquet(pepperoniBouquet);
        birthBouquet.setBouquet(weddingBouquet);
        birthBouquet1.setBouquet(birthBouquet);
        delivery.setBouquet(birthBouquet1);
        discount.setBouquet(delivery);

        BouquetDecorator fullBouquet = discount;

        double price = fullBouquet.getCost();
        String name = fullBouquet.getName();
        List<String> toppings = fullBouquet.getToppings();

        System.out.println("price: " + price);
        System.out.println("name: " + name);
        System.out.println("CONSIST OF ===================");
        for (String component : toppings) {
            System.out.println(component);
        }
    }
}
