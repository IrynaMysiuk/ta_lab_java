package com.epam.lab.patterns.behaviour.impl;

public class Ticket {

    private static int counter = 0;
    private int productNumber;

    public Ticket() {
        counter++;
        productNumber = counter;
    }

    @Override
    public String toString() {
        return "Ticket" + productNumber;
    }
}

