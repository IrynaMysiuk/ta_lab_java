package com.epam.lab.patterns.behaviour.impl;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.Color;
import com.epam.lab.patterns.behaviour.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class Order {
    private static Logger logger = LogManager.getLogger(Application.class);

    private State state;
    private List<Ticket> tickets = new LinkedList<>();

    public Order() {
        state = new NewTicketState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void addTicket() {
        state.addTicket(this);
    }

    public void moveToInProgress() {
        if (tickets.isEmpty()) {
            logger.info("Order is empty!");
        } else {
            state.moveToInProgress(this);
        }
    }

    public void moveToPeerReview() {
        state.moveToPeerReview(this);
    }

    public void moveToTest() {
        state.moveToInTest(this);
    }

    public void moveToDone() {
        state.moveToDone(this);
    }

    public void moveToBlocked() {
        state.moveToBlocked(this);
    }

    public void addTicketToList() {
        Ticket ticket = new Ticket();
        tickets.add(ticket);
        logger.info(Color.RED_BOLD + "" + ticket + Color.RESET + " add to Bucket");
    }

    public void getTicketList() {
        logger.info(tickets);
    }
}

