package com.epam.lab.patterns.behaviour.impl;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.behaviour.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class NewTicketState implements State {
    private static Logger logger = LogManager.getLogger(Application.class);

    @Override
    public void addTicket(Order order) {
        order.addTicketToList();
    }

    @Override
    public void moveToInProgress(Order order) {
        order.setState(new InProgressState());
        logger.info("Order is in progress");
    }

    @Override
    public void moveToBlocked(Order order) {
        order.setState(new BlockedState());
        logger.info("Order is blocked");
    }
}