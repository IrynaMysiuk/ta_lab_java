package com.epam.lab.patterns.behaviour.impl;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.behaviour.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InTestState implements State {
    private static Logger logger = LogManager.getLogger(Application.class);

    @Override
    public void moveToDone(Order order) {
        order.setState(new DoneState());
        logger.info("Order is done");
    }
}