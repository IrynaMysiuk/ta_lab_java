package com.epam.lab.patterns.behaviour;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.Color;
import com.epam.lab.patterns.behaviour.impl.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class App {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        Order order = new Order();
        do {
            logger.info(Color.BLUE);
            logger.info("   1 - Add Ticket" + "   2 - In progress   ");
            logger.info("   3 - Peer review   " + "   4 - In test    ");
            logger.info("   5 - Done          " + "   6 - Blocked     ");
            logger.info("   L - Get Ticket List" + "   Q - exit");
            logger.info("   Please, select menu point.");
            logger.info(Color.RESET);
            keyMenu = input.nextLine().toUpperCase();
            switch (keyMenu) {
                case "1":
                    order.addTicket();
                    break;
                case "2":
                    order.moveToInProgress();
                    break;
                case "3":
                    order.moveToPeerReview();
                    break;
                case "4":
                    order.moveToTest();
                    break;
                case "5":
                    order.moveToDone();
                    break;
                case "6":
                    order.moveToBlocked();
                    break;
                case "L":
                    order.getTicketList();
                    break;
            }
        } while (!keyMenu.equals("Q"));
    }
}
