package com.epam.lab.patterns.behaviour;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.behaviour.impl.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {
    Logger logger = LogManager.getLogger(Application.class);

    default void addTicket(Order order) {
        logger.info("addTicket - is not allowed");
    }

    default void moveToInProgress(Order order) {
        logger.info("In progress - is not allowed");
    }

    default void moveToPeerReview(Order order) {
        logger.info("Peer review - is not allowed");
    }

    default void moveToInTest(Order order) {
        logger.info("In Test - is not allowed");
    }

    default void moveToDone(Order order) {
        logger.info("Done - is not allowed");
    }

    default void moveToBlocked(Order order) {
        logger.info("Blocked - is not allowed");
    }
}