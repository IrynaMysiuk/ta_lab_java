package com.epam.lab.patterns.creational.main;

public enum PizzaComponents {
    THICK,
    THIN,
    CRUST,
    MARINARA,
    PLUM_TOMATO,
    PESTO,
    SAUSAGES,
    MUSHROOMS,
    OLIVES,
    PARMESAN,
    MOZZARELLA
}
