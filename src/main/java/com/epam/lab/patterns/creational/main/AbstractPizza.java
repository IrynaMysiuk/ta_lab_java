package com.epam.lab.patterns.creational.main;

import com.epam.lab.patterns.creational.Location;
import com.epam.lab.patterns.creational.pizza.Pizza;

public abstract class AbstractPizza {
    protected abstract Pizza createPizza(PizzaType pizzaType);

    public Pizza order(Location location, PizzaType pizzaType) {
        Pizza pizza = createPizza(pizzaType);
        pizza.prepare(location)
                .bake(location)
                .cut()
                .box()
                .build();
        return pizza;
    }
}

