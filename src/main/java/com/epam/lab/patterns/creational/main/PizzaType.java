package com.epam.lab.patterns.creational.main;

public enum PizzaType {
    CHEESE,
    VEGGIE,
    CLAM,
    PEPPERONI
}
