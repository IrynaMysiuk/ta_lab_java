package com.epam.lab.patterns.creational;


import com.epam.lab.patterns.creational.main.Main;
import com.epam.lab.patterns.creational.main.PizzaType;

public class Demo {

    public static void main(String[] args) {
        Main main = new Main();
        main.output(Location.LVIV, PizzaType.CHEESE);
        main.output(Location.LVIV, PizzaType.CLAM);
        main.output(Location.LVIV, PizzaType.PEPPERONI);
        main.output(Location.LVIV, PizzaType.VEGGIE);

        main.output(Location.KYIV, PizzaType.CHEESE);
        main.output(Location.KYIV, PizzaType.CLAM);
        main.output(Location.KYIV, PizzaType.PEPPERONI);
        main.output(Location.KYIV, PizzaType.VEGGIE);

        main.output(Location.DNIPRO, PizzaType.CHEESE);
        main.output(Location.DNIPRO, PizzaType.CLAM);
        main.output(Location.DNIPRO, PizzaType.PEPPERONI);
        main.output(Location.DNIPRO, PizzaType.VEGGIE);

    }
}

