package com.epam.lab.patterns.creational.pizza;


import com.epam.lab.patterns.creational.Location;
import com.epam.lab.patterns.creational.main.PizzaComponents;

import java.util.ArrayList;
import java.util.List;

public interface Pizza {
    List<PizzaComponents> comp = new ArrayList<>();

    Pizza prepare(Location location);

    Pizza bake(Location location);

    Pizza cut();

    Pizza box();

    void build();
}
