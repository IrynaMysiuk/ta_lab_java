package com.epam.lab.patterns.creational.main;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.creational.Location;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Application.class);

    public void output(Location location, PizzaType pizzaType) {
        logger.info("You need ordering a pizza of type- " + pizzaType.toString() +
                ", baked by- " + location.toString() + "!");
        AbstractPizza pizzeria = new Pizzeria();
        pizzeria.order(location, pizzaType);
    }
}
