package com.epam.lab.patterns.creational.pizza.impl;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.creational.Location;
import com.epam.lab.patterns.creational.main.PizzaComponents;
import com.epam.lab.patterns.creational.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PizzaVeggie implements Pizza {
    private static Logger logger = LogManager.getLogger(Application.class);

    @Override
    public Pizza prepare(Location location) {
        List<String> resultPrepare = new ArrayList<>();
        if (location == Location.LVIV) {
            comp.add(PizzaComponents.CRUST);
            comp.add(PizzaComponents.MARINARA);
        } else if (location == Location.KYIV) {
            comp.add(PizzaComponents.THIN);
            comp.add(PizzaComponents.PESTO);
        } else if (location == Location.DNIPRO) {
            comp.add(PizzaComponents.THICK);
            comp.add(PizzaComponents.PLUM_TOMATO);
        }
        return this;
    }

    @Override
    public Pizza bake(Location location) {
        if (location == Location.LVIV) {
            comp.add(PizzaComponents.SAUSAGES);
        } else if (location == Location.KYIV) {
            comp.add(PizzaComponents.MOZZARELLA);
        } else if (location == Location.DNIPRO) {
            comp.add(PizzaComponents.PARMESAN);
        }
        logger.info(this.getClass().getSimpleName() +
                " Your order complete with toppings: " + comp);
        return this;
    }

    @Override
    public Pizza cut() {
        logger.info(this.getClass().getSimpleName() +
                " Cut");
        return this;
    }

    @Override
    public Pizza box() {
        logger.info(this.getClass().getSimpleName() + " Box");
        return this;
    }

    @Override
    public void build() {
        logger.info("Your order is done! Pizza Veggie consists of " +
                comp.stream().map(Enum::name).collect(Collectors.toList()));
        comp.clear();
    }
}

