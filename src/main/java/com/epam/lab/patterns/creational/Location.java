package com.epam.lab.patterns.creational;

public enum Location {
    LVIV,
    KYIV,
    DNIPRO
}
