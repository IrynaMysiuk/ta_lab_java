package com.epam.lab.patterns.creational.pizza.impl;

import com.epam.lab.logger.Application;
import com.epam.lab.patterns.creational.Location;
import com.epam.lab.patterns.creational.main.PizzaComponents;
import com.epam.lab.patterns.creational.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.Collectors;

public class PizzaCalm implements Pizza {
    private static Logger logger = LogManager.getLogger(Application.class);

    @Override
    public PizzaCalm prepare(Location location) {
        if (location == Location.LVIV) {
            comp.add(PizzaComponents.THICK);
            comp.add(PizzaComponents.PLUM_TOMATO);

        } else if (location == Location.KYIV) {
            comp.add(PizzaComponents.CRUST);
            comp.add(PizzaComponents.MARINARA);
        } else if (location == Location.DNIPRO) {
            comp.add(PizzaComponents.THICK);
            comp.add(PizzaComponents.PESTO);
        } else {
            logger.info("Not allowed Bakery!!");
        }
        return this;
    }

    @Override
    public PizzaCalm bake(Location location) {
        if (location == Location.LVIV) {
            comp.add(PizzaComponents.OLIVES);
        } else if (location == Location.KYIV) {
            comp.add(PizzaComponents.SAUSAGES);
        } else if (location == Location.DNIPRO) {
            comp.add(PizzaComponents.MUSHROOMS);
        } else {
            logger.warn("Not allowed Bakery!!");
        }
        logger.info(this.getClass().getSimpleName() +
                " Your order was baked with toppings: " + comp);
        return this;
    }

    @Override
    public Pizza cut() {
        logger.info(this.getClass().getSimpleName() + "Cut");
        return this;
    }

    @Override
    public Pizza box() {
        logger.info(this.getClass().getSimpleName() + "Box");
        return this;
    }

    @Override
    public void build() {
        logger.info("Your order is done! Pizza Calm consists of " +
                comp.stream().map(Enum::name).collect(Collectors.toList()));
        comp.clear();
    }

}
