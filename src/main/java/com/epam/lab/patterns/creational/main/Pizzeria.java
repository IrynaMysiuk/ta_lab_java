package com.epam.lab.patterns.creational.main;

import com.epam.lab.patterns.creational.pizza.Pizza;
import com.epam.lab.patterns.creational.pizza.impl.PizzaCalm;
import com.epam.lab.patterns.creational.pizza.impl.PizzaCheese;
import com.epam.lab.patterns.creational.pizza.impl.PizzaPepperoni;
import com.epam.lab.patterns.creational.pizza.impl.PizzaVeggie;

public class Pizzeria extends AbstractPizza {

    protected Pizza createPizza(PizzaType pizzaType) {
        Pizza pizza = null;
        if (pizzaType == PizzaType.CHEESE) {
            pizza = new PizzaCheese();
        } else if (pizzaType == PizzaType.CLAM) {
            pizza = new PizzaCalm();
        } else if (pizzaType == PizzaType.PEPPERONI) {
            pizza = new PizzaPepperoni();
        } else if (pizzaType == PizzaType.VEGGIE) {
            pizza = new PizzaVeggie();
        }
        return pizza;
    }
}


