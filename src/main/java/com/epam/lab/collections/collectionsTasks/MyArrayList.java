package com.epam.lab.collections.collectionsTasks;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class MyArrayList {
    private String[] myArray;
    private int count;
    private int outputCount;
    static Logger logger = LogManager.getLogger(Application.class);

    MyArrayList() {
        this.count = 0;
        this.myArray = new String[Constants.SIZE];
        this.outputCount = 1;
    }

    public void get() {
        for (String value : myArray) {
            if (value != null) {
                logger.info(String.format("arr[%d]=%s;", outputCount++, value));
            }
        }
    }

    public void add(String text) {
        myArray[count] = text;
        count++;
        if (count == myArray.length)
            resize(2 * myArray.length);
    }

    private int timeRunningJavaArrayList() {
        List<String> arrayList = new ArrayList<>();
        long start = System.currentTimeMillis();
        for (int i = 0; i < Constants.MAXVALUE; i++)
            arrayList.add("Hi");
        long stop = System.currentTimeMillis();
        return (int) (stop - start);
    }

    private int timeRunningMyArrayList() {
        MyArrayList arrList = new MyArrayList();
        long start = System.currentTimeMillis();
        for (int i = 0; i < Constants.MAXVALUE; i++)
            arrList.add("Hi");
        long stop = System.currentTimeMillis();
        return (int) (stop - start);
    }

    private void resize(double capacity) {
        String[] copy = new String[(int) capacity];
        System.arraycopy(myArray, 0, copy, 0, myArray.length);
        myArray = copy;
    }

    public void compare() {
        logger.info("\n-----------TASK 4-----------\n");
        logger.info(String.format("Implementation time to add to MyArrayList = %d;", timeRunningMyArrayList()));
        logger.info(String.format("Implementation time to add to ArrayList in Java = %d;", timeRunningJavaArrayList()));
    }
}
