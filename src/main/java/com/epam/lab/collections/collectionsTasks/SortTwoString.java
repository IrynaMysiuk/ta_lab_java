package com.epam.lab.collections.collectionsTasks;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortTwoString {
    private String Country;
    private String Capital;
    private static Logger logger = LogManager.getLogger(Application.class);

    public SortTwoString(String Country, String Capital) {
        super();
        this.Country = Country;
        this.Capital = Capital;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getCapital() {
        return Capital;
    }

    public void setCapital(String Capital) {
        this.Capital = Capital;
    }

    public static Comparator<SortTwoString> CountryComparator = (country1, country2) -> {
        String countryName1 = country1.getCountry().toUpperCase();
        String countryName2 = country2.getCountry().toUpperCase();
        return countryName1.compareTo(countryName2);
    };
    public static Comparator<SortTwoString> CapitalComparator = (capital1, capital2) -> {

        String capitalName1 = capital1.getCapital().toUpperCase();
        String capitalName2 = capital2.getCapital().toUpperCase();
        return capitalName1.compareTo(capitalName2);
    };

    private static void output(SortTwoString[] country) {
        int i = 0;
        logger.info("\nSort first String:");
        for (SortTwoString temp : country) {
            logger.info(String.format("country %d : %s", ++i, temp.getCountry()));
        }
        logger.info("\nSort second String:");
        Arrays.sort(country, SortTwoString.CapitalComparator);
        i = 0;
        for (SortTwoString temp : country) {
            logger.info(String.format("country %d : %s", ++i, temp.getCapital()));
        }
    }

    private static void binarySearch(SortTwoString[] country, String countryName, String capitalName,
                                     Comparator<SortTwoString> comparator) {
        List<SortTwoString> names = Arrays.asList(country);
        int idx = Collections.binarySearch(names, new SortTwoString(countryName, capitalName), comparator);
        if (comparator == CountryComparator)
            logger.info(String.format("Found '%s' at index: %d", countryName, (idx + 1)));
        if (comparator == CapitalComparator)
            logger.info(String.format("Found '%s' at index: %d", capitalName, Math.abs(idx + 1)));
    }

    public static void main() {
        logger.info("\n-----------TASK 5-----------\n");
        SortTwoString[] country = new SortTwoString[4];
        country[0] = new SortTwoString("Bulgaria", "Sophie");
        country[1] = new SortTwoString("Ukraine", "Kyiv");
        country[2] = new SortTwoString("Luxembourg", "Luxembourg");
        country[3] = new SortTwoString("Turkey", "Antalia");
        Arrays.sort(country, SortTwoString.CountryComparator);
        binarySearch(country, "Ukraine", "Kyiv", CountryComparator);
        binarySearch(country, "Ukraine", "Kyiv", CapitalComparator);
        output(country);
    }
}
