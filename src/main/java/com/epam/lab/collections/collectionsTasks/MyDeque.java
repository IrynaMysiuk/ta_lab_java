package com.epam.lab.collections.collectionsTasks;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyDeque<Item> implements Iterable<Item> {
    private int size;
    private allNodes first;
    private allNodes last;
    static Logger logger = LogManager.getLogger(Application.class);

    public MyDeque() {
    }

    public void addFirst(Item item) {
        if (item == null)
            throw new NullPointerException();
        allNodes newFirst = new allNodes();
        newFirst.item = item;
        if (first != null) {
            newFirst.next = first;
            first.previous = newFirst;
        }
        first = newFirst;
        if (last == null)
            last = first;
        increaseSize();
    }

    public Item removeFirst() {
        if (first == null)
            throw new NoSuchElementException();
        allNodes oldFirst = first;
        first = first.next;
        if (first == null)
            last = null;
        else
            first.previous = null;
        reductionSize();
        return oldFirst.item;
    }

    public void addLast(Item item) {
        if (item == null)
            throw new NullPointerException();
        allNodes newLast = new allNodes();
        newLast.item = item;
        if (last != null) {
            newLast.previous = last;
            last.next = newLast;
        }
        last = newLast;
        if (first == null)
            first = last;
        increaseSize();
    }

    public Item removeLast() {
        if (first == null)
            throw new NoSuchElementException();
        allNodes oldLast = last;
        last = oldLast.previous;
        if (last == null)
            first = null;
        else
            last.next = null;
        reductionSize();
        return oldLast.item;
    }

    public boolean isEmpty() {
        return this.first == null;
    }

    public int size() {
        return size;
    }

    private int increaseSize() {
        return size++;
    }

    private int reductionSize() {
        return size--;
    }

    private class allNodes {
        Item item;
        allNodes next;
        allNodes previous;
    }

    @Override
    public Iterator<Item> iterator() {
        return new ItemsIterator();
    }

    private class ItemsIterator implements Iterator<Item> {

        private allNodes current;

        public ItemsIterator() {
            current = first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (current == null)
                throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

    public static void test() {
        logger.info("\n-----------TASK 6-----------\n");
        MyDeque<Integer> mydeque = new MyDeque<>();
        mydeque.addFirst(2);
        mydeque.addLast(4);
        mydeque.addFirst(6);
        mydeque.removeLast();
        mydeque.size();
        for (int value : mydeque) {
            logger.info(String.format("\nNumber in Deque: %d", value));
        }
    }
}
