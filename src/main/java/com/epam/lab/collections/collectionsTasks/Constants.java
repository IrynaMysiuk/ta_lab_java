package com.epam.lab.collections.collectionsTasks;

public class Constants {
    public static int SIZE = 10;
    public static int MAXVALUE = 1000000;

    private Constants() {
    }
}
