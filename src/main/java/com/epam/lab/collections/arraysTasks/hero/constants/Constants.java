package com.epam.lab.collections.arraysTasks.hero.constants;


public class Constants {
    private Constants() {
    }

    public static final int HERO_POWER = 25;
    public static final int MAX_DOORS = 10;
    public static final int LOW_MAGIC_POINT = 10;
    public static final int HIGH_MAGIC_POINT = 80;
    public static final int LOW_MONSTER_POINT = 5;
    public static final int HIGH_MONSTER_POINT = 100;
    public static final String HORIZONTAL_LINE = "-----------------------";
    public static final String HORIZONTAL_HEADER = "---Object---|---Power--";
}
