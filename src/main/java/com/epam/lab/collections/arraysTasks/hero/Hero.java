package com.epam.lab.collections.arraysTasks.hero;

public class Hero {
    private int heroPower;

    public void setHeroPower(int heroPower) {
        this.heroPower = heroPower;
    }

    public int getHeroPower() {
        return heroPower;
    }
}
