package com.epam.lab.collections.arraysTasks.hero;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

import static com.epam.lab.collections.arraysTasks.hero.constants.Constants.*;

public class CircleRoom {
    static Logger logger = LogManager.getLogger(Application.class.getName());
    private Hero hero = new Hero();
    private Random random = new Random();
    private boolean[] isBattles = new boolean[MAX_DOORS];

    private int getMagicArtifact() {
        return random.nextInt(HIGH_MAGIC_POINT - LOW_MAGIC_POINT) + LOW_MAGIC_POINT;
    }

    private int getMonsterPower() {
        return random.nextInt(HIGH_MONSTER_POINT - LOW_MONSTER_POINT) + LOW_MONSTER_POINT;
    }

    public void chooseDoor(int doorNumber) {
        if (isBattles[doorNumber]) {
            hero.setHeroPower(HERO_POWER + getMagicArtifact());
            logger.info(HORIZONTAL_HEADER);
            logger.info("|Magic artifact|\\t\" + hero.getHeroPower() + \"\\t|");
            logger.info(HORIZONTAL_LINE);
        } else {
            int monsterPower = getMonsterPower();
            logger.info(HORIZONTAL_HEADER);
            logger.info("|\\tMonster\\t|\\t\" + monsterPower + \"\\t|");
            logger.info(HORIZONTAL_LINE);
            performBattle(monsterPower);
        }
    }

    public void performBattle(int monsterPower) {
        if (hero.getHeroPower() >= monsterPower) {
            logger.info("You`re winner!");
        } else {
            logger.info("Game over! Your power is less than the monster");
            System.exit(0);
        }
    }

    public void calcBattles() {
        for (int doorNumber = 0; doorNumber < MAX_DOORS; doorNumber++) {
            isBattles[doorNumber] = random.nextBoolean();
        }
    }

    public void calcDangerousDoors() {
        int counter = 0;
        for (boolean isBattle : isBattles) {
            if (!isBattle) {
                counter++;
            }
        }
        logger.info("Dangerous doors founded: " + counter);
    }
}
