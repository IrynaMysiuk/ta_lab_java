package com.epam.lab.collections.arraysTasks;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeleteDuplicate {
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public static int removeDuplicateElements(int[] myArray, int number) {
        if (number == 0 || number == 1) {
            return number;
        }
        int j = 0;
        for (int i = 0; i < number - 1; i++) {
            if (myArray[i] != myArray[i + 1]) {
                myArray[j++] = myArray[i];
            }
        }
        myArray[j++] = myArray[number - 1];
        return j;
    }

    public static void main(String[] args) {
        int[] arr = {10, 20, 20, 30, 30, 40, 50, 50};
        int length = arr.length;
        length = removeDuplicateElements(arr, length);
        for (int i = 0; i < length; i++)
            logger.info(arr[i] + " ");
    }
}
