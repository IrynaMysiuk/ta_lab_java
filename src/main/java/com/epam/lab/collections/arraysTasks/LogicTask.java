package com.epam.lab.collections.arraysTasks;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogicTask {
    static int[] firstArray = {1, 2, 3, 4};
    static int[] secondArray = {1, 2, 3};
    static int[] thirdArray;
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public LogicTask() {
        thirdArray = new int[firstArray.length + secondArray.length];
    }

    public void compareArray() {
        int counter = 0;
        for (int firstArrayValue : firstArray) {
            logger.info(firstArrayValue + " ");
            thirdArray[counter] = firstArrayValue;
            counter++;
        }
        System.out.println();
        for (int secondArrayValue : secondArray) {
            logger.info(secondArrayValue + " ");
            thirdArray[counter] = secondArrayValue;
            counter++;
        }
        System.out.println();
        for (int thirdArrayValue : thirdArray) {
            logger.info(thirdArrayValue + " ");
        }
    }

    public void compareNotExistedArray() {
        int counter = 0;
        for (int firstArrayValue : firstArray) {
            logger.info(firstArrayValue + " ");
            thirdArray[counter] = firstArrayValue;
            counter++;
        }
        for (int thirdArrayValue : thirdArray) {
            for (int secondArrayValue : secondArray) {
                logger.info(secondArrayValue + " ");
                if (thirdArrayValue != secondArrayValue) {
                    logger.info(thirdArrayValue + " ");
                }
            }
        }
    }

    public static void main(String[] args) {
        LogicTask logicTask = new LogicTask();
        logicTask.compareArray();
        logicTask.compareNotExistedArray();
    }
}

