package com.epam.lab.collections.arraysTasks.hero;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static com.epam.lab.collections.arraysTasks.hero.constants.Constants.MAX_DOORS;

public class Demo {
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {
        CircleRoom circleRoom = new CircleRoom();
        int[] actualDoorNumbers = new int[MAX_DOORS];
        int doorNumber;
        logger.info("WELCOME IN GAME!\nLet's open the door!");
        circleRoom.calcBattles();
        circleRoom.calcDangerousDoors();
        for (int counter = 0; counter < MAX_DOORS; counter++) {
            logger.info("Input door number from 1 to 10");
            Scanner scanner = new Scanner(System.in);
            doorNumber = scanner.nextInt();
            if (doorNumber < 1) {
                throw new RuntimeException("You entered less than possible number!");
            } else if (doorNumber > MAX_DOORS) {
                throw new RuntimeException("You entered more than possible number!");
            }
            for (int actualDoorNumber : actualDoorNumbers) {
                if (actualDoorNumber == doorNumber) {
                    throw new RuntimeException("You input existed door number!");
                }
            }
            actualDoorNumbers[counter] = doorNumber;
            circleRoom.chooseDoor(actualDoorNumbers[counter] - 1);
        }
    }
}
