package com.epam.lab.string.task1;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringOptions {
    private static Logger logger = LogManager.getLogger(Application.class);

    public void getSplit(String str) {
        String[] split = str.split("the|you");
        for (String splited : split) {
            logger.info("Split sentence : " + splited);
        }
    }

    public void replaceVowelsToUnderscores(String str) {
        String replace = str.replaceAll("[aeiou]", "_");
        logger.info("Replace sentence:" + replace);
    }
}
