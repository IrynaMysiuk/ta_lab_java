package com.epam.lab.string.task1;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public void checkRegex(List<String> lines) {
        for (String line : lines) {
            Pattern pattern = Pattern.compile("(^[A-Z].*\\.$)");
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                System.out.println("group 1: " + matcher.group(1));
            }
        }
    }
}