package com.epam.lab.string.task1;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Demo {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info(StringUtils.join("Hello ", "Iryna ", "!"));

        InternalizationMenu stringConverter = new InternalizationMenu();
        logger.info("Entered number from 1 to 3:");
        Scanner scanner = new Scanner(System.in);
        int languageNumber = scanner.nextInt();
        stringConverter.chooseLanguage(languageNumber);

        List<String> lines = Arrays.asList(
                "articles For History of Engineering & Technology.",
                "Visit Us Now! Fresh & Original Articles.",
                "Professional Satisfaction",
                "Thousands Of Article.");
        Regex regex = new Regex();
        regex.checkRegex(lines);

        StringOptions stringOptions = new StringOptions();
        stringOptions.getSplit("the world. you wonderful");
        stringOptions.replaceVowelsToUnderscores("the world.you wonderful");
    }
}
