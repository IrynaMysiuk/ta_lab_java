package com.epam.lab.string.task1;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;

public class InternalizationMenu {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void getLocaleInfo(Locale current) {
        logger.info("Country: " + current.getCountry());
        logger.info("Region: " + current.getDisplayCountry());
        logger.info("ID region: " + current.getLanguage());
        logger.info("Language region: "
                + current.getDisplayLanguage());
        logger.info(" ");
    }

    public void chooseLanguage(int languageNumber) {
        switch (languageNumber) {
            case 1: {
                getLocaleInfo(Locale.CANADA_FRENCH);
                break;
            }
            case 2: {
                getLocaleInfo(Locale.FRANCE);
                break;
            }
            case 3: {
                getLocaleInfo(Locale.ENGLISH);
                break;
            }
        }
    }
}
