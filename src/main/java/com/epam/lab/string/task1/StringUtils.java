package com.epam.lab.string.task1;


public class StringUtils {

    public StringUtils() {
    }

    public static String join(String... strings) {
        StringBuilder joinedString = new StringBuilder("");
        for (String string : strings) {
            joinedString.append(string);
        }
        return joinedString.toString();
    }

}
