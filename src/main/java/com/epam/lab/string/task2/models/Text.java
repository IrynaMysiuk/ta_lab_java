package com.epam.lab.string.task2.models;

import java.util.Arrays;
import java.util.List;

import static com.epam.lab.string.task2.utils.Constants.REGEX_POINTS_AND_EXCLAMATION_MARK;
import static com.epam.lab.string.task2.utils.Constants.REGEX_SPLIT_POINTS;

public class Text {
    public List<String> splitSentence(String text) {
        return Arrays.asList(text.split(REGEX_SPLIT_POINTS));
    }

    public List<String> splitByQuestionMark(String text) {
        return Arrays.asList(text.split(REGEX_POINTS_AND_EXCLAMATION_MARK));
    }
}
