package com.epam.lab.string.task2.models;

import java.util.*;
import java.util.stream.Collectors;

import static com.epam.lab.string.task2.utils.Constants.REGEX_SPACE;
import static com.epam.lab.string.task2.utils.Constants.STARTS_WITH_VOWELS;

public class Sentence {

    public List<String> splitWord(String sentence) {
        return Arrays.asList(sentence.split(REGEX_SPACE));
    }

    public long getWordNumber(String sentence) {
        return sentence.split(REGEX_SPACE).length;
    }

    public String getLongestWord(List<String> words) {
        return words.stream().max(Comparator.comparing(String::length)).get();
    }

    public List<String> getFixedSizeWords(List<String> words, int length) {
        return words.stream().filter(word -> word.length() != length
                && !word.matches(STARTS_WITH_VOWELS)).collect(Collectors.toList());
    }

    public List<String> sortWords(List<String> words) {
        return words.stream()
                .filter(s -> !s.isEmpty())
                .sorted(Comparator.comparing(String::toString))
                .collect(Collectors.toList());
    }

    public List<String> getUniqueWords(List<String> words) {
        return words.stream().distinct().collect(Collectors.toList());
    }

    public Map<String, Long> sortMapByWordNumbers(Map<String, Long> sentencesMap) {
        return sentencesMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
    }

    public Map<String, String> sortByConsonantLetters(Map<String, String> consonantWordsMap) {
        return consonantWordsMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}
