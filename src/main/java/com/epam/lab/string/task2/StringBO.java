package com.epam.lab.string.task2;

import com.epam.lab.string.task2.models.Sentence;
import com.epam.lab.string.task2.models.Text;
import com.epam.lab.string.task2.models.Word;

import java.util.*;
import java.util.stream.Collectors;

import static com.epam.lab.string.task2.utils.Constants.*;

public class StringBO {

    private Text text = new Text();
    private Sentence sentence = new Sentence();
    private Word word = new Word();

    public Set<String> replaceQuestionSentence(String text, int maxLength) {
        List<String> sentences = this.text.splitByQuestionMark(text);
        Set<String> result = new HashSet<>();
        for (String sentence : sentences) {
            String question = sentence.split(REGEX_QUESTION_MARK)[0];
            String[] words = question.split(REGEX_SPACE);
            for (String word : words) {
                if (word.length() == maxLength) {
                    result.add(word);
                }
            }
        }
        return result;
    }

    public int getUniqueWordsNumber(String text) {
        List<String> sentences = this.text.splitSentence(text);
        int uniqueWordsCounter = 0;
        for (String sentence : sentences) {
            List<String> words = this.sentence.splitWord(sentence);
            List<String> uniqueWords = this.sentence.getUniqueWords(words);
            if (uniqueWords.size() == words.size()) {
                uniqueWordsCounter++;
            }
        }
        return uniqueWordsCounter;
    }

    public Set<String> sortByWordNumber(String text) {
        List<String> sentences = this.text.splitSentence(text);
        Map<String, Long> sentencesMap = new HashMap<>();
        for (String sentence : sentences) {
            long wordNumber = this.sentence.getWordNumber(sentence);
            sentencesMap.put(sentence, wordNumber);
        }
        Map<String, Long> sortedByWords = this.sentence.sortMapByWordNumbers(sentencesMap);
        return sortedByWords.keySet();
    }

    public String checkSimilarWord(String text) {

        List<String> sentences = this.text.splitSentence(text);
        List<String> firstSentenceWords = this.sentence.splitWord(sentences.get(0));
        String finalText = text.replace(sentences.get(0), EMPTY);
        return firstSentenceWords.stream()
                .filter(word -> !finalText.contains(word))
                .findFirst().get();
    }

    public List<String> replaceVowelsWordToLongestWord(String text) {
        List<String> sentences = this.text.splitSentence(text);
        List<String> result = new ArrayList<>();
        for (String sentence : sentences) {
            List<String> words = this.sentence.splitWord(sentence);
            String longestWord = this.sentence.getLongestWord(words);
            for (String word : words) {
                if (word.matches(REGEX_VOWELS)) {
                    sentence = sentence.replace(word, longestWord);
                    result.add(sentence);
                    break;
                }
            }
        }
        return result;
    }

    public List<String> sortWordToFirstLetter(String text) {
        List<String> words = word.getWords(text);
        words = this.sentence.sortWords(words);
        return word.outputNewLetterOnNewLine(words);
    }

    public Set<String> getWordToVowelsLetterSortToConsonant(String text) {
        List<String> words = word.getWords(text);
        List<String> result = word.getWordsStartsWithVowels(words);
        Map<String, String> consonantWordsMap = new HashMap<>();
        List<String> consonantList = result.stream()
                .map(e -> e.replaceAll(VOWELS_LETTERS, EMPTY))
                .collect(Collectors.toList());
        for (int i = 0; i < result.size(); i++)
            consonantWordsMap.put(result.get(i), consonantList.get(i));
        Map<String, String> sortedConsonantMap = this.sentence.sortByConsonantLetters(consonantWordsMap);
        return sortedConsonantMap.keySet();
    }

    public Set<String> getSortPercentageOfVowels(String text) {
        List<String> words = word.getWords(text);
        Map<String, Long> result = word.getPercentageConsonantWords(words);
        Map<String, Long> sortedByWords = this.sentence.sortMapByWordNumbers(result);
        return sortedByWords.keySet();
    }

    public List<String> deleteFixedSizeWordToConsonantWord(String text) {
        List<String> sentences = this.text.splitSentence(text);
        List<String> fixedSizeWords = new ArrayList<>();
        List<String> words = new ArrayList<>();
        for (String sentence : sentences) {
            words.addAll(this.sentence.splitWord(sentence));
            fixedSizeWords.addAll(this.sentence.getFixedSizeWords(words, FIXED_LENGTH));
        }
        return fixedSizeWords;
    }

    public Set<String> sortedFixedLetterAsc(String text) {
        List<String> words = word.getWords(text);
        Map<String, Long> wordByLetterMap = new HashMap<>();
        for (String word : words)
            wordByLetterMap.put(word, (long) word.replaceAll(REGEX_ONLY_O, EMPTY).length());
        Map<String, Long> sortedByLetterAsc = sentence.sortMapByWordNumbers(wordByLetterMap);
        return sortedByLetterAsc.keySet();
    }

    public List<String> sortedByFixedListDesc(String text, List<String> fixedListWords) {
        List<String> words = word.getWords(text);
        Map<String, Long> wordCounterMap = word.getWordCounterMap(words, fixedListWords);
        Map<String, Long> sortedWordCounterMap = sentence.sortMapByWordNumbers(wordCounterMap);
        List<String> reversedSortWordCounter = new ArrayList<>(sortedWordCounterMap.keySet());
        Collections.reverse(reversedSortWordCounter);
        return reversedSortWordCounter;
    }

    public String getMaxLengthPalindrome(String text) {
        List<String> words = word.getWords(text);
        return words.stream()
                .filter(Word::isPalindrome)
                .filter(s -> s.length() > 1)
                .max(Comparator.comparing(String::length))
                .get();
    }

    public List<String> deleteNextByFirstLetter(String text) {
        List<String> allWordsOfText = word.getWords(text);
        return allWordsOfText.stream()
                .filter(s -> !s.isEmpty())
                .map(this::deleteNextOfFirstLetter)
                .collect(Collectors.toList());
    }

    public String deleteNextOfFirstLetter(String word) {
        List<Character> wordChars = this.word.getChars(word);
        Character first = wordChars.remove(0);
        List<Character> charsWithRemovedFirstLetter = wordChars.stream()
                .filter(character -> !character.equals(first)).
                        collect(Collectors.toList());
        charsWithRemovedFirstLetter.add(0, first);
        return charsWithRemovedFirstLetter.stream()
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}
