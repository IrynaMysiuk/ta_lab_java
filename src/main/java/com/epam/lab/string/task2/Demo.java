package com.epam.lab.string.task2;

import com.epam.lab.string.task2.utils.ConsoleOutput;
import com.epam.lab.string.task2.utils.TxtReader;

public class Demo {

    public static void main(String[] args) throws Exception {
        TxtReader txtReader = new TxtReader();
        ConsoleOutput consoleOutput = new ConsoleOutput(txtReader.readText());
        consoleOutput.outputUniqueWords();
        consoleOutput.outputSortedWordsByLength();
        consoleOutput.outputUniqueFirstWordSentence();
        consoleOutput.outputLongestWordStarsWithVowels();
        consoleOutput.outputQuestionSentenceWithLength();
        consoleOutput.outputSortedByConsonantLetter();
        consoleOutput.outputSortSentenceByFirstWord();
        consoleOutput.outputFixedSizeWords();
        consoleOutput.outputSortedByPercentageVowels();
        consoleOutput.outputSortByLetterAsc();
        consoleOutput.outputSortByFixedListDesc();
        consoleOutput.outputLongestPalindrome();
        consoleOutput.outputDeleteNextLettersByFirst();
    }
}
