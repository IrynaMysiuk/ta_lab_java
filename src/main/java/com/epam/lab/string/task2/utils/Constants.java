package com.epam.lab.string.task2.utils;

public class Constants {
    private Constants() {
    }

    public static final String PATH_TO_FILE = "src\\main\\resources\\ProgrammingBook.txt";
    public static final int MAX_LENGTH_WORD = 9;
    public static final String REGEX_SPLIT_POINTS = "\\.";
    public static final String REGEX_SPACE = " ";
    public static final String REGEX_QUESTION_MARK = "\\?";
    public static final String REGEX_POINTS_AND_EXCLAMATION_MARK = "[.!]";
    public static final String REGEX_VOWELS = "^[aioeuAIOEU].*";
    public static final String REGEX_CONSONANT = "[^aioeuAIOEU]";
    public static final String STARTS_WITH_VOWELS = "\\b[aeiou]\\w*";
    public static final String VOWELS_LETTERS = "[aoueiAOUEI]";
    public static final String SPACE_AND_TABULATE = "[\\s+|\t]";
    public static final String EMPTY = "";
    public static final int FIXED_LENGTH = 5;
    public static final int PERCENT = 100;
    public static final String REGEX_ONLY_O = "[^oO]{1,}";

}
