package com.epam.lab.string.task2.utils;

import com.epam.lab.logger.Application;
import com.epam.lab.string.task2.StringBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

import static com.epam.lab.string.task2.utils.Constants.MAX_LENGTH_WORD;

public class ConsoleOutput {
    private static Logger logger = LogManager.getLogger(Application.class);
    private StringBO stringBO = new StringBO();
    private String text;

    public ConsoleOutput(String text) {
        this.text = text;
    }

    public void outputUniqueWords() {
        logger.info("\nUnique words: " +
                stringBO.getUniqueWordsNumber(text));
    }

    public void outputSortedWordsByLength() {
        logger.info("\nSorted by word number: " +
                stringBO.sortByWordNumber(text));
    }

    public void outputUniqueFirstWordSentence() {
        logger.info("\nUnique Word in first sentence: " +
                stringBO.checkSimilarWord(text));
    }

    public void outputQuestionSentenceWithLength() {
        logger.info("\n Output fixed size words: " +
                stringBO.replaceQuestionSentence(text, MAX_LENGTH_WORD));
    }

    public void outputLongestWordStarsWithVowels() {
        logger.info("\n Replace first vowels word to longest word: " +
                stringBO.replaceVowelsWordToLongestWord(text));
    }

    public void outputSortSentenceByFirstWord() {
        logger.info("\nSort by first word: " +
                stringBO.sortWordToFirstLetter(text));
    }

    public void outputSortedByConsonantLetter() {
        logger.info("\nSort by consonant letter: " +
                stringBO.getWordToVowelsLetterSortToConsonant(text));
    }


    public void outputSortedByPercentageVowels() {
        logger.info("\nSort by percentage vowels letter: " +
                stringBO.getSortPercentageOfVowels(text));
    }

    public void outputFixedSizeWords() {
        logger.info("\n Remove all words fixed size that start in consonant letter: "
                + stringBO.deleteFixedSizeWordToConsonantWord(text));
    }

    public void outputSortByLetterAsc() {
        logger.info("\n Sort by letter asc: " + stringBO.sortedFixedLetterAsc(text));
    }

    public void outputSortByFixedListDesc() {
        List<String> fixedListWords = Arrays.asList("the", "a", "or", "are", "is", "as");
        logger.info("\n Sort by fixed list desc: " + stringBO.sortedByFixedListDesc(text, fixedListWords));
    }

    public void outputLongestPalindrome() {
        logger.info("\n Palindrome with maximum length: " + stringBO.getMaxLengthPalindrome(text));
    }

    public void outputDeleteNextLettersByFirst() {
        logger.info("\n Delete next by first letter: " + stringBO.deleteNextByFirstLetter(text));
    }
}


