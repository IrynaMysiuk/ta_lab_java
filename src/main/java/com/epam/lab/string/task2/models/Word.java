package com.epam.lab.string.task2.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.epam.lab.string.task2.utils.Constants.*;

public class Word {

    private Sentence sentence = new Sentence();
    private Text text = new Text();

    public static boolean isPalindrome(String word) {
        String lowerCaseWord = word.toLowerCase();
        int firstCounter = 0;
        int secondCounter = lowerCaseWord.length() - 1;
        while (firstCounter < secondCounter) {
            if (lowerCaseWord.charAt(firstCounter) != lowerCaseWord.charAt(secondCounter))
                return false;
            firstCounter++;
            secondCounter--;
        }
        return true;
    }

    public List<String> getWords(String text) {
        List<String> sentences = this.text.splitSentence(text);
        List<String> words = new ArrayList<>();
        for (String sentence : sentences) {
            words.addAll(this.sentence.splitWord(sentence));
        }
        return words;
    }

    public List<Character> getChars(String word) {
        return word.toLowerCase().chars()
                .mapToObj(ch -> (char) ch).collect(Collectors.toList());
    }

    public Map<String, Long> getWordCounterMap(List<String> words, List<String> fixedListWords) {
        Map<String, Long> wordCounterMap = new HashMap<>();
        for (String word : words) {
            if (fixedListWords.contains(word)) {
                if (wordCounterMap.containsKey(word)) {
                    wordCounterMap.put(word, wordCounterMap.get(word) + 1);
                } else {
                    wordCounterMap.put(word, 1L);
                }
            }
        }
        return wordCounterMap;
    }

    public Map<String, Long> getPercentageConsonantWords(List<String> words) {
        Map<String, Long> result = new HashMap<>();
        for (String word : words)
            if (!word.isEmpty())
                result.put(word, (long) ((long) word.replaceAll(REGEX_CONSONANT, EMPTY).length() /
                        (double) word.length() * PERCENT));
        return result;
    }

    public List<String> getWordsStartsWithVowels(List<String> words) {
        List<String> result = new ArrayList<>();
        for (String word : words) {
            if (word.matches(STARTS_WITH_VOWELS)) {
                result.add(word);
            }
        }
        return result;
    }

    public List<String> outputNewLetterOnNewLine(List<String> words) {
        List<String> result = new ArrayList<>();
        char previousFirstLetter = words.get(0).charAt(0);
        for (String word : words) {
            if (previousFirstLetter != word.charAt(0))
                result.add("\n" + word);
            else
                result.add(word);
            previousFirstLetter = word.charAt(0);
        }
        return result;
    }
}
