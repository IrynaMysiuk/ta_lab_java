package com.epam.lab.string.task2.utils;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static com.epam.lab.string.task2.utils.Constants.*;

public class TxtReader {
    private static Logger logger = LogManager.getLogger(Application.class);

    public String readText() throws IOException {
        File file = new File(PATH_TO_FILE);
        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;
        StringBuilder formattedText = new StringBuilder();
        while ((line = br.readLine()) != null) {
            formattedText.append(line.replaceAll(SPACE_AND_TABULATE, REGEX_SPACE));
            logger.info(line);
        }
        logger.info("\nYour text replace tabulate and double space to space!");
        return formattedText.toString();
    }
}
