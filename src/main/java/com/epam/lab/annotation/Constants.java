package com.epam.lab.annotation;

public class Constants {
    public final static String[] argStr = {"Apples", "Raspberries", "Bananas", "Grapes"};
    public final static int[] argInt = {22, 12, 13, 41};
    public final static String myName = "Iryna Mysiuk";
    public final static String myCity = "Lviv";
    public final static int myAge = 20;
}
