package com.epam.lab.annotation;


import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Demo {
    static Logger logger = LogManager.getLogger(Application.class.getName());

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    @interface MyAnnotation {
        int value();
    }

    public static void main(String[] args) throws SecurityException, NoSuchFieldException, ClassNotFoundException {
        logger.info("--------Print into console----------");
        PrintIntoConsole printIntoConsole = new PrintIntoConsole();
        printIntoConsole.outputTask3();
        logger.info("-------- Invoke method----------");
        InvokeAnnotation invokeAnnotation = new InvokeAnnotation();
        invokeAnnotation.getInvoke();
        logger.info("--------Print unknown type----------");
        UnknownType unknownType = new UnknownType();
        unknownType.getUnknownType();
        logger.info("--------Print unknown type and show all information----------");
        ObjectUnknownType objectUnknownType = new ObjectUnknownType();
        objectUnknownType.getUnknownType();
        logger.info("--------Own Annotation----------");
        Class<?> cls = Class.forName("com.epam.lab.annotation.OwnAnnotation");
        OwnAnnotation ownAnnotation = new OwnAnnotation(cls);
        ownAnnotation.getClassName();
        ownAnnotation.allInfoMethods();
        ownAnnotation.allInfoConstructors();
        ownAnnotation.allInfoFields();
    }
}

