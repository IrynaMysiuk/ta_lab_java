package com.epam.lab.annotation;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class UnknownType {
    public static int a;
    public static int b;
    static Logger logger = LogManager.getLogger(Application.class.getName());

    UnknownType() {
        UnknownType.a = 1;
        UnknownType.b = 1;
    }

    private void printInfo(Field field) throws IllegalArgumentException, IllegalAccessException {
        logger.info("name = " + field.getName());
        logger.info("decl class = " + field.getDeclaringClass());
        logger.info("type = " + field.getType());
        logger.info("modifiers = " + Modifier.toString(field.getModifiers()));
    }

    public void getUnknownType() {
        Class<?> cls;
        try {
            cls = Class.forName("com.epam.lab.annotation.UnknownType");
            Field fieldlist[] = cls.getDeclaredFields();
            for (int i = 0; i < fieldlist.length; i++) {
                if (fieldlist[i].getType().equals(int.class)) {
                    fieldlist[i].set(cls, Constants.myAge + i);
                }
                printInfo(fieldlist[i]);
                logger.info("get: " + fieldlist[i].get(cls));
            }
        } catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
