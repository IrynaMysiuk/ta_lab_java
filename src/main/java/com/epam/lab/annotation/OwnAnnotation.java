package com.epam.lab.annotation;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class OwnAnnotation {
    public Class<?> cls;
    static Logger logger = LogManager.getLogger(Application.class.getName());

    OwnAnnotation(Class<?> className) {
        this.cls = className;
    }

    private void getMethodParameters(Method method) {
        Class<?> pvec[] = method.getParameterTypes();
        for (int j = 0; j < pvec.length; j++)
            logger.info(" param #" + j + " " + pvec[j]);
        Class<?> evec[] = method.getExceptionTypes();
        for (int j = 0; j < evec.length; j++)
            logger.info("exc #" + j + " " + evec[j]);
    }

    private void getConstuctorParamreters(Constructor<?> ct) {
        Class<?> pvec[] = ct.getParameterTypes();
        for (int j = 0; j < pvec.length; j++)
            logger.info("param #" + j + " " + pvec[j]);
        Class<?> evec[] = ct.getExceptionTypes();
        for (int j = 0; j < evec.length; j++)
            logger.info("exc #" + j + " " + evec[j]);
    }

    public void allInfoMethods() {
        try {
            Method methList[] = cls.getDeclaredMethods();
            for (Method method : methList) {
                logger.info(
                        String.format("name  = %s;\ndecl class = %s", method.getName(), method.getDeclaringClass()));
                getMethodParameters(method);
                logger.info(String.format("return type = %s\n-----", method.getReturnType()));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }


    public void allInfoConstructors() {
        try {
            Constructor<?> ctorList[] = cls.getDeclaredConstructors();
            for (Constructor<?> ct : ctorList) {
                System.out
                        .println(String.format("name  = %s;\ndecl class = %s;", ct.getName(), ct.getDeclaringClass()));
                getConstuctorParamreters(ct);
                logger.info("-----");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void allInfoFields() {
        try {
            Field fieldList[] = cls.getDeclaredFields();
            for (Field field : fieldList) {
                logger.info("name   = " + field.getName());
                logger.info("decl class = " + field.getDeclaringClass());
                logger.info("type   = " + field.getType());
                logger.info("modifiers = " + Modifier.toString(field.getModifiers()));
                logger.info("-----");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void getClassName() {
        logger.info(String.format("Class name: %s\n", cls.getName()));
    }
}


