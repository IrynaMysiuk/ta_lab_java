package com.epam.lab.generics;

public class Constants {
    private Constants() {

    }

    public static final int LOW_SALARY = 20000;
    public static final int HIGH_SALARY = 30000;
    public static final int TWO_PEOPLE = 2;
    public static final int THOUSAND_PEOPLE = 100;
    public static final int TEN_PEOPLE = 10;
    public static final int STANDARD_SALARY = 2000;
    public static final int TWENTY_PEOPLE = 20;
    public static final int THIRD_PEOPLE = 30;
    public static final int FORTH_PEOPLE = 40;
}
