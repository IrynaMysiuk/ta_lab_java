package com.epam.lab.generics;

class Warship<T extends PeopleOnBoard> {

    private T value;

    public Warship(T obj) {
        value = obj;
    }

    public String getName() {
        return value.getName();
    }

    public int getNumber() {
        return value.getNumber();
    }

    public int getSalary() {
        return value.getSalary();
    }

    public boolean isEqual(Warship<? super Military> other) {
        boolean result;
        result = value.getName().equals(other.getName()) && value.getNumber() == other.getNumber()
                && value.getSalary() == other.getSalary();
        return result;
    }
}