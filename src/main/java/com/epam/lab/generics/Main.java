package com.epam.lab.generics;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.lab.generics.Constants.*;

public class Main {
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {
        logger.info("-------------Task 1-------------");
        Warship<Captain> cap = new Warship<>(new Captain("Frank", LOW_SALARY, TWO_PEOPLE));
        Warship<Military> groupMilitary1 = new Warship<>(new Military("Group1", HIGH_SALARY, THOUSAND_PEOPLE));
        Warship<Military> groupMilitary2 = new Warship<>(new Military("Group1", HIGH_SALARY, THOUSAND_PEOPLE));
        Warship<Civilian> civilian = new Warship<>(new Civilian("Common People", LOW_SALARY, TEN_PEOPLE));
        logger.info(cap.getSalary());
        logger.info(groupMilitary1.getSalary());
        logger.info(groupMilitary2.getSalary());
        logger.info(civilian.getSalary());
        logger.info(String.format("Is salary same? %s", cap.isEqual(groupMilitary1)));
        logger.info(String.format("Is salary same? %s", cap.isEqual(groupMilitary2)));
        logger.info(String.format("Is salary same? %s", groupMilitary1.isEqual(groupMilitary2)));
        logger.info("-------------Task 2-------------");
        LinkedList<Captain> list = new LinkedList<>();
        list.add(new LinkedListNode<>(new Captain("Frank", STANDARD_SALARY, TEN_PEOPLE)));
        list.add(new LinkedListNode<>(new Captain("Tom", STANDARD_SALARY, TWENTY_PEOPLE)));
        LinkedList<Military> list1 = new LinkedList<>();
        list1.add(new LinkedListNode<>(new Military("Group1", STANDARD_SALARY, TEN_PEOPLE)));
        list1.add(new LinkedListNode<>(new Military("Group2", STANDARD_SALARY, TWENTY_PEOPLE)));
        list1.add(new LinkedListNode<>(new Military("Group3", STANDARD_SALARY, THIRD_PEOPLE)));
        list1.add(new LinkedListNode<>(new Military("Group4", STANDARD_SALARY, FORTH_PEOPLE)));
        list.print();
        list.remove();
        logger.info("After removing the head..");
        list.print();
        list1.print();
        list1.remove();
        list1.remove();
        logger.info("After removing the head..");
        list1.print();
    }
}
