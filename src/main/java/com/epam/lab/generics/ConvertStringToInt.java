package com.epam.lab.generics;

import java.util.ArrayList;
import java.util.List;

public class ConvertStringToInt {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        @SuppressWarnings("rawtypes")
        List strLst = new ArrayList<>(); // List and ArrayList holds Objects
        strLst.add(1);
        strLst.add(2);
        strLst.add(3);
        strLst.add("Iryna"); // Compiler/runtime cannot detect this error
        // compile okay, but runtime ClassCastException if you write: Integer a = (Integer) strLst.get(3);
    }
}
