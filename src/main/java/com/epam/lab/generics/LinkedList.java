package com.epam.lab.generics;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LinkedList<T extends PeopleOnBoard> {

    private LinkedListNode<T> first = null;
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public void addFirst(LinkedListNode<T> node) {
        node.setNext(first);
        first = node;
    }

    public void add(LinkedListNode<T> node) {
        node.setNext(first);
        first = node;
    }

    public void remove() {
        if (first.getNext() != null)
            first = first.getNext();
    }

    private void printList(LinkedListNode<T> node) {
        logger.info(
                String.format("Name: %s; Salary: %s; Number: %s;",
                        node.getName(), node.getSalary(), node.getNumber()));
        if (node.getNext() != null)
            printList(node.getNext());
    }

    public void print() {
        if (first != null)
            printList(first);
    }
}

class LinkedListNode<T> {
    private String name;
    private int salary;
    private int number;
    private LinkedListNode<T> next;

    public LinkedListNode(Captain value) {
        this.name = value.getName();
        this.salary = value.getSalary();
        this.number = value.getNumber();
    }

    public LinkedListNode(Military value) {
        this.name = value.getName();
        this.salary = value.getSalary();
        this.number = value.getNumber();
    }

    public void setNext(LinkedListNode<T> next) {
        this.next = next;
    }

    public LinkedListNode<T> getNext() {
        return next;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public int getNumber() {
        return number;
    }
}
