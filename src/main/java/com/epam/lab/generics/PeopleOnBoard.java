package com.epam.lab.generics;

public class PeopleOnBoard {

    private String name;
    private int salary;
    private int number;

    public PeopleOnBoard(String name, int sal, int number) {
        this.name = name;
        this.salary = sal;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}