package com.epam.lab.jdbc.entities;

public class Employee {

    private int idEmployee;
    private String firstName;
    private String lastName;
    private String middleName;
    private String point;
    private String birthdayDay;
    private String phone;
    private String passport;
    private String recordBookNumber;
    private String arrivalDate;
    private String parentsName;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String middleName,
                    String point, String birthdayDay, String parentsName, String phone, String passport,
                    String recordBookNumber, String arrivalDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.point = point;
        this.birthdayDay = birthdayDay;
        this.parentsName = parentsName;
        this.phone = phone;
        this.passport = passport;
        this.recordBookNumber = recordBookNumber;
        this.arrivalDate = arrivalDate;
    }

    public int getId() {
        return idEmployee;
    }

    public void setId(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getFirstName() {
        return firstName;
    }

    public Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Employee setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getPoint() {
        return point;
    }

    public Employee setPoint(String point) {
        this.point = point;
        return this;
    }

    public String getBirthdayDay() {
        return birthdayDay;
    }

    public Employee setBirthdayDay(String birthdayDay) {
        this.birthdayDay = birthdayDay;
        return this;
    }

    public String getParentsName() {
        return parentsName;
    }

    public Employee setParentsName(String parentsName) {
        this.parentsName = parentsName;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Employee setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPassport() {
        return passport;
    }

    public Employee setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    public String getRecordBookNumber() {
        return recordBookNumber;
    }

    public Employee setRecordBookNumber(String recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
        return this;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public Employee setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
        return this;
    }
}
