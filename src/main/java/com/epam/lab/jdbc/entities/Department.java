package com.epam.lab.jdbc.entities;

public class Department {

    private int idSpeciality;
    private String specialityName;
    private String describeSpeciality;

    public Department() {
    }

    public Department(String specialityName, String describeSpeciality) {
        this.specialityName = specialityName;
        this.describeSpeciality = describeSpeciality;
    }

    public int getId() {
        return idSpeciality;
    }

    public void setId(int idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public String getDescribeSpeciality() {
        return describeSpeciality;
    }

    public void setDescribeSpeciality(String describeSpeciality) {
        this.describeSpeciality = describeSpeciality;
    }
}