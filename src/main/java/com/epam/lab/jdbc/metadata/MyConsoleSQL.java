package com.epam.lab.jdbc.metadata;

import com.epam.lab.jdbc.util.ConnectionConfiguration;
import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class MyConsoleSQL {
    private static Logger logger = LogManager.getLogger(Application.class);

    public MyConsoleSQL() {
    }

    public void getAllColumnMetaData(String tableName) {
        Connection connection = ConnectionConfiguration.getConnection();
        Statement st;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery(String.format("SELECT * FROM %s", tableName));
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            System.out.println(columnsNumber + String.format
                    ("\nmysql> SELECT * FROM %s;\n---------------------------------------------", tableName));
            int count = 1;
            while (rs.next()) {
                if (count == columnsNumber + 1) {
                    count = 1;
                }
                System.out.print(rs.getString(count) + "|");
                count++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getColumnsNameMetaData(String tableName) {
        Connection connection;
        DatabaseMetaData md;
        try {
            connection = ConnectionConfiguration.getConnection();
            md = connection.getMetaData();
            ResultSet rset = md.getColumns(null, null, tableName, null);
            logger.info(String.format
                    ("mysql> SHOW COLUMNS FROM %s;\n---------------------------------------------", tableName));
            while (rset.next()) {
                System.out.print(rset.getInt("ORDINAL_POSITION") + "|");
                System.out.print(rset.getString(4) + "|");
                System.out.print(rset.getString("TYPE_NAME") + "|");
                System.out.print(rset.getBoolean("NULLABLE") + "|");
                System.out.print(rset.getInt("COLUMN_SIZE") + "|");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getTablesMetaData() {
        Connection connection;
        try {
            connection = ConnectionConfiguration.getConnection();
            DatabaseMetaData dbmd = connection.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            logger.info("mysql> SHOW TABLES;\n--------------");
            while (rs.next()) {
                logger.info(rs.getString("TABLE_NAME"));
            }
            logger.info("--------------");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
