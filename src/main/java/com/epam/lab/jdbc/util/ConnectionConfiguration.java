package com.epam.lab.jdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionConfiguration {

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection
                    ("jdbc:mysql://localhost:3306/employee_task?autoReconnect=true&useSSL=false",
                            "mysiuk", "1111");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}