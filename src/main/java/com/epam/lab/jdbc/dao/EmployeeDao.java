package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.entities.Employee;

import java.util.List;

public interface EmployeeDao {

    void createEmployeeTable();

    void insert(Employee employee);

    Employee selectById(int id);

    List<Employee> selectAll();

    void delete(int id);

    void update(Employee employee, int id);
}