package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.entities.Department;

import java.util.List;

public interface DepartmentDao {

    void createDepartmentTable();

    void insert(Department department);

    Department selectById(int id);

    List<Department> selectAll();

    void delete(int id);

    void update(Department department, int id);
}

