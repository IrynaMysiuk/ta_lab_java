package com.epam.lab.jdbc.daoimpl;

import com.epam.lab.jdbc.dao.EmployeeDao;
import com.epam.lab.jdbc.entities.Employee;
import com.epam.lab.jdbc.util.ConnectionConfiguration;
import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaolmpl implements EmployeeDao {
    private static Logger logger = LogManager.getLogger(Application.class);

    public void createEmployeeTable() {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS employees (idEmployee int primary key unique auto_increment," +
                    "lastName varchar(55), firstName varchar(55), middleName varchar(255),point varchar(1),birthdayDay varchar(25)," +
                    "parentsName varchar(255),phone int,passport varchar(25),record_book_number varchar(25),arrival_date varchar(25));");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void insert(Employee employee) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO employees " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, employee.getId());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setString(3, employee.getFirstName());
            preparedStatement.setString(4, employee.getMiddleName());
            preparedStatement.setString(5, employee.getPoint());
            preparedStatement.setString(6, employee.getBirthdayDay());
            preparedStatement.setString(7, employee.getParentsName());
            preparedStatement.setString(8, employee.getPhone());
            preparedStatement.setString(9, employee.getPassport());
            preparedStatement.setString(10, employee.getRecordBookNumber());
            preparedStatement.setString(11, employee.getArrivalDate());
            preparedStatement.executeUpdate();
            logger.info("INSERT INTO employee" + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Employee selectById(int id) {
        Employee employee = new Employee();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM employees WHERE idEmployee = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employee.setId(resultSet.getInt("idEmployee"));
                employee.setLastName(resultSet.getString("lastName"));
                employee.setFirstName(resultSet.getString("firstName"));
                employee.setMiddleName(resultSet.getString("middleName"));
                employee.setPoint(resultSet.getString("point"));
                employee.setBirthdayDay(resultSet.getString("birthdayDay"));
                employee.setParentsName(resultSet.getString("parentsName"));
                employee.setPhone(resultSet.getString("phone"));
                employee.setPassport(resultSet.getString("passport"));
                employee.setRecordBookNumber(resultSet.getString("record_book_number"));
                employee.setArrivalDate(resultSet.getString("arrival_date"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return employee;
    }

    public List<Employee> selectAll() {
        List<Employee> employees = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM employees");
            while (resultSet.next()) {
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("idEmployee"));
                employee.setLastName(resultSet.getString("lastName"));
                employee.setFirstName(resultSet.getString("firstName"));
                employee.setMiddleName(resultSet.getString("middleName"));
                employee.setPoint(resultSet.getString("point"));
                employee.setBirthdayDay(resultSet.getString("birthdayDay"));
                employee.setParentsName(resultSet.getString("parentsName"));
                employee.setPhone(resultSet.getString("phone"));
                employee.setPassport(resultSet.getString("passport"));
                employee.setRecordBookNumber(resultSet.getString("record_book_number"));
                employee.setArrivalDate(resultSet.getString("arrival_date"));
                employees.add(employee);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return employees;
    }

    public void delete(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM employees WHERE idEmployee = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.info("DELETE FROM employee WHERE idEmployee = ?");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void update(Employee employee, int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE employees SET " +
                    "lastName = ?, firstName = ?, middleName = ?, point = ?, birthdayDay = ?, parentsName =?, phone = ?, passport = ?," +
                    "record_book_number = ?, arrival_date = ?" + " WHERE idEmployee = ?");
            preparedStatement.setString(1, employee.getLastName());
            preparedStatement.setString(2, employee.getFirstName());
            preparedStatement.setString(3, employee.getMiddleName());
            preparedStatement.setString(4, employee.getPoint());
            preparedStatement.setString(5, employee.getBirthdayDay());
            preparedStatement.setString(6, employee.getParentsName());
            preparedStatement.setString(7, employee.getPhone());
            preparedStatement.setString(8, employee.getPassport());
            preparedStatement.setString(9, employee.getRecordBookNumber());
            preparedStatement.setString(10, employee.getArrivalDate());
            preparedStatement.setInt(11, id);
            preparedStatement.executeUpdate();
            logger.info("UPDATE employees SET " +
                    "firstName = ?, lastName = ?, middleName = ?, point = ?, birthdayDay = ?, phone = ?, passport = ?," +
                    "record_book_number = ?, arrival_date = ? WHERE idEmployee = ?");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

