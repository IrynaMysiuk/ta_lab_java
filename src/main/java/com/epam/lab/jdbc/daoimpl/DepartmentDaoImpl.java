package com.epam.lab.jdbc.daoimpl;

import com.epam.lab.jdbc.dao.DepartmentDao;
import com.epam.lab.jdbc.entities.Department;
import com.epam.lab.jdbc.util.ConnectionConfiguration;
import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentDaoImpl implements DepartmentDao {
    private static Logger logger = LogManager.getLogger(Application.class);

    public void createDepartmentTable() {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS department (idSpeciality int primary key unique auto_increment, " +
                    "speciality_name varchar(55), describe_speciality varchar(55))");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void insert(Department department) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO department " + "VALUES (?, ?, ?)");
            preparedStatement.setInt(1, department.getId());
            preparedStatement.setString(2, department.getSpecialityName());
            preparedStatement.setString(3, department.getDescribeSpeciality());
            preparedStatement.executeUpdate();
            logger.info("INSERT INTO department " + "VALUES (?, ?, ?)");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Department selectById(int id) {
        Department department = new Department();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM department WHERE idSpeciality = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                department.setId(resultSet.getInt("idSpeciality"));
                department.setSpecialityName(resultSet.getString("speciality_name"));
                department.setDescribeSpeciality(resultSet.getString("describe_speciality"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return department;
    }

    public List<Department> selectAll() {
        List<Department> departments = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM department");
            while (resultSet.next()) {
                Department department = new Department();
                department.setId(resultSet.getInt("idSpeciality"));
                department.setSpecialityName(resultSet.getString("speciality_name"));
                department.setDescribeSpeciality(resultSet.getString("describe_speciality"));
                departments.add(department);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return departments;
    }

    public void delete(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM department WHERE idSpeciality = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.info("DELETE FROM department WHERE idSpeciality = ?");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void dropTableWithCopy() {
        Connection connection = ConnectionConfiguration.getConnection();
        Statement st;
        try {
            st = connection.createStatement();
            st.execute("CREATE TABLE IF NOT EXISTS temporary_department AS (SELECT * FROM department WHERE speciality_name='ER')");
            st.execute("UPDATE temporary_department SET speciality_name='ФеІ', describe_speciality='електроніки' WHERE idSpeciality=999");
            st.execute("DELETE FROM temporary_department WHERE speciality_name='ER'");
            int rows = st.executeUpdate("INSERT INTO department SELECT * FROM temporary_department");
            if (rows == 0) {
                logger.info("Don't add any row!");
            } else {
                logger.info(rows + " row(s)affected.");
                connection.close();
            }
            st.execute("DROP TABLE temporary_department");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Department department, int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE department SET " +
                    "speciality_name = ?, describe_speciality = ? WHERE idSpeciality = ?");
            preparedStatement.setString(1, department.getSpecialityName());
            preparedStatement.setString(2, department.getDescribeSpeciality());
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();
            logger.info("UPDATE department SET " +
                    "speciality_name = ?, describe_speciality = ? WHERE idSpeciality = ?");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
