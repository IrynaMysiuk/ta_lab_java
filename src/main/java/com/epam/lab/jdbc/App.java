package com.epam.lab.jdbc;

import com.epam.lab.jdbc.daoimpl.DepartmentDaoImpl;
import com.epam.lab.jdbc.daoimpl.EmployeeDaolmpl;
import com.epam.lab.jdbc.entities.Department;
import com.epam.lab.jdbc.entities.Employee;
import com.epam.lab.jdbc.metadata.MyConsoleSQL;
import com.epam.lab.jdbc.transactions.MyTransaction;
import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class App {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws SQLException {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Create table");
        sdi.createEmployeeTable();
        logger.info("Insert a new record");
        Employee employee = new Employee()
                .setLastName("Лопатка").setFirstName("Остап")
                .setMiddleName("Северинович").setPoint("ч")
                .setBirthdayDay("1995-12-01").setParentsName("Лопатка Северин Васильович")
                .setPhone("11122233").setPassport("СС0909СС")
                .setRecordBookNumber("3114049с").setArrivalDate("2016-09-01");
        sdi.insert(employee);
        logger.info("Select by id");
        Employee employeeSelect = sdi.selectById(2);
        logger.info(employee.getId() + ", " + employee.getFirstName() + ", " + employee.getLastName());
        logger.info("Delete person by id");
        sdi.delete(2);
        logger.info("Update person");
        Employee personUpdate = new Employee()
                .setLastName("Грабля").setFirstName("Кирило")
                .setMiddleName("Васильович").setPoint("ч")
                .setBirthdayDay("1995-12-01").setParentsName("Грабля Василь Орестович")
                .setPhone("2221133").setPassport("ДД0909ДД")
                .setRecordBookNumber("3114079с").setArrivalDate("2018-09-01");
        sdi.update(personUpdate, 1);
        logger.info("Select all persons");
        List<Employee> employees = sdi.selectAll();
        for (Employee p : employees) {
            System.out.println((p.getId() + ", " + p.getFirstName() + ", " + p.getLastName()));
        }
        DepartmentDaoImpl ddi = new DepartmentDaoImpl();
        logger.info("Create table");
        ddi.createDepartmentTable();
        logger.info("Insert a new record");
        Department department = new Department("ФеІ", "електроніки");
        ddi.insert(department);
        logger.info("Select by id");
        Department departmentSelect = ddi.selectById(2);
        logger.info(department.getId() + ", " + department.getSpecialityName() + ", " + department.getDescribeSpeciality());
        logger.info("Delete person by id");
        ddi.delete(1);
        logger.info("Delete with copy");
        ddi.dropTableWithCopy();
        logger.info("Update person");
        Department departmentUpdate = new Department("КН", "комп'ютерні науки");
        ddi.update(departmentUpdate, 1);
        logger.info("Select all persons");
        List<Department> departments = ddi.selectAll();
        for (Department p : departments) {
            System.out.println((p.getId() + ", " + p.getSpecialityName() + ", " + p.getDescribeSpeciality()));
        }
        logger.info("Transaction");
        MyTransaction transaction = new MyTransaction();
        transaction.getTransaction();
        MyConsoleSQL consoleSQL = new MyConsoleSQL();
        consoleSQL.getTablesMetaData();
        consoleSQL.getColumnsNameMetaData("department");
        consoleSQL.getAllColumnMetaData("department");
    }
}