package com.epam.lab.flowerStore.flowerpots;

import com.epam.lab.flowerStore.base.Products;

import static com.epam.lab.flowerStore.base.Constants.PRICE_ORCHIDS;

public class Orchids extends Flowerpots {
    private int price;


    public int getPrice() {

        if (price == 0) {
            price = PRICE_ORCHIDS;
        }
        return price;
    }

    public Products setPrice(int price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Orchids{" +
                '\'' + " price=" + getPrice() +
                ", number=" + getNumbers() +
                ", Total result=" + getTotalPrice() + ", color=" + getColor() + '}';
    }

}
