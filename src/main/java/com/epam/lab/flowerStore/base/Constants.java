package com.epam.lab.flowerStore.base;

public interface Constants {
    public static final int PRICE_TABLE_FLOWERS = 50;
    public static final int PRICE_TAPE = 3;
    public static final int PRICE_WRAP_FLOWER = 14;
    public static final int PRICE_DAISIES = 5;
    public static final int PRICE_LEMONS = 500;
    public static final int PRICE_ORCHIDS = 300;
    public static final int PRICE_VIOLETS = 76;
    public static final int ADMIN_PASSWORD = 1111;
    public static final int NUMBER_OF_PRODUCTS = 4;
}
