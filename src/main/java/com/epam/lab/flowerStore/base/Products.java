package com.epam.lab.flowerStore.base;


public abstract class Products {


    private int numbers;
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(Colors color) {
        this.color = color.name();
        System.out.println("You enter color:" + color);
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
        System.out.println("You enter number:\n" + numbers);
    }

    public abstract int getPrice();

    public abstract Products setPrice(int price);


    public int getTotalPrice() {
        return getNumbers() * getPrice();
    }

}


