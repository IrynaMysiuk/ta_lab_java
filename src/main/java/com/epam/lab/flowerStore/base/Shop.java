package com.epam.lab.flowerStore.base;

import com.epam.lab.flowerStore.decors.TablesFlowers;
import com.epam.lab.flowerStore.decors.Tape;
import com.epam.lab.flowerStore.decors.WrapFlower;
import com.epam.lab.flowerStore.flowerpots.Lemons;
import com.epam.lab.flowerStore.flowerpots.Orchids;
import com.epam.lab.flowerStore.flowerpots.Violets;
import com.epam.lab.flowerStore.flowers.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static com.epam.lab.flowerStore.base.Constants.ADMIN_PASSWORD;
import static com.epam.lab.flowerStore.base.Constants.NUMBER_OF_PRODUCTS;
import static com.epam.lab.flowerStore.flowers.Flowers.*;

public class Shop {


    private List<Products> products;
    Scanner scanner;

    public Shop() {
        products = new ArrayList<>();
        scanner = new Scanner(System.in);
    }

    private void setPriceByAdmin(Products products) {
        System.out.println("Do you want change price?\nY/N");
        String answer = scanner.next();
        if (answer.equalsIgnoreCase("Y")) {
            System.out.println("Enter password admin:");
            int currentPassword = scanner.nextInt();
            if (currentPassword == ADMIN_PASSWORD) {
                System.out.println("Success password");
                System.out.println("Please input price:");
                products.setPrice(scanner.nextInt());
            } else {
                System.out.println("Try again");
            }
        }
    }

    public void chooseFlowers() {
        for (int i = 0; i < NUMBER_OF_PRODUCTS; i++) {
            System.out.println("Please choose flowers:\n1.Daisies\n2.Roses\n3.Lilies\n4.Exit");
            int choiceFlowers = scanner.nextInt();
            switch (choiceFlowers) {
                case 1: {
                    Daisies daisies = addDaisies();
                    System.out.println("Enter number of daisies:");
                    int choiceDaisies = scanner.nextInt();
                    daisies.setNumbers(choiceDaisies);
                    daisies.setColor(Colors.WHITE);
                    setPriceByAdmin(daisies);
                    products.add(daisies);
                    break;
                }
                case 2: {
                    Roses roses = addRoses();
                    System.out.println("Enter number of roses:");
                    int choiceDaisies = scanner.nextInt();
                    roses = chooseRosesSort(roses);
                    roses.setNumbers(choiceDaisies);
                    roses = (Roses) chooseColors(roses);
                    setPriceByAdmin(roses);
                    products.add(roses);
                    break;
                }
                case 3: {
                    Lilies lilies = addLilies();
                    System.out.println("Enter number of lilies:");
                    int choiceDaisies = scanner.nextInt();
                    lilies = chooseLiliaSort(lilies);
                    lilies.setNumbers(choiceDaisies);
                    lilies = (Lilies) chooseColors(lilies);
                    setPriceByAdmin(lilies);
                    products.add(lilies);
                    break;
                }
            }
        }
        products.sort(new SortByPrice());
        for (Products flower : products) {
            System.out.println(flower);
        }
        sumProductPrice(products);
        sumProductNumber(products);
    }

    static class SortByPrice implements Comparator<Products> {
        public int compare(Products product1, Products product2) {
            return product1.getTotalPrice() - product2.getTotalPrice();
        }
    }

    public Roses chooseRosesSort(Roses roses) {
        System.out.println("Please, choose sort of rose:\n1.Bush\n2.Mini\n3.Patio\n4.Scrabble\n5.Tea");
        int chooseRose = scanner.nextInt();
        switch (chooseRose) {
            case 1: {
                roses.setSort(RoseSort.BUSH);
                break;
            }
            case 2: {
                roses.setSort(RoseSort.MINI);
                break;
            }
            case 3: {
                roses.setSort(RoseSort.PATIO);
                break;
            }
            case 4: {
                roses.setSort(RoseSort.SCRABBLE);
                break;
            }
            case 5: {
                roses.setSort(RoseSort.TEA);
                break;
            }
        }
        return roses;
    }

    public Lilies chooseLiliaSort(Lilies lilies) {
        System.out.println("Please, choose sort of lilia:\n1.American\n2.Asian\n3.Curly\n4.Royal");
        int chooseLilia = scanner.nextInt();
        switch (chooseLilia) {
            case 1: {
                lilies.setLiliaSort(LiliaSort.AMERICAN);
                break;
            }
            case 2: {
                lilies.setLiliaSort(LiliaSort.ASIAN);
                break;
            }
            case 3: {
                lilies.setLiliaSort(LiliaSort.CURLY);
                break;
            }
            case 4: {
                lilies.setLiliaSort(LiliaSort.ROYAL);
                break;
            }
        }
        return lilies;
    }

    public void chooseFlowerpots() {
        for (int i = 0; i < NUMBER_OF_PRODUCTS; i++) {
            System.out.println("Please choose flowers:\n1.Lemons\n2.Orchids\n3.Violets\n4.Exit");
            int choiceFlowerpots = scanner.nextInt();
            switch (choiceFlowerpots) {
                case 1: {
                    Lemons lemons = new Lemons();
                    System.out.println("Enter number of lemons:");
                    int choiceLemons = scanner.nextInt();
                    lemons.setNumbers(choiceLemons);
                    lemons.setColor(Colors.YELLOW);
                    setPriceByAdmin(lemons);
                    products.add(lemons);
                    break;
                }
                case 2: {
                    Orchids orchids = new Orchids();
                    System.out.println("Enter number of orchids:");
                    int choiceOrchids = scanner.nextInt();
                    orchids.setNumbers(choiceOrchids);
                    orchids = (Orchids) chooseColors(orchids);
                    setPriceByAdmin(orchids);
                    products.add(orchids);
                    break;
                }
                case 3: {
                    Violets violets = new Violets();
                    System.out.println("Enter number of violets:");
                    int choiceViolets = scanner.nextInt();
                    violets.setNumbers(choiceViolets);
                    violets = (Violets) chooseColors(violets);
                    setPriceByAdmin(violets);
                    products.add(violets);
                    break;
                }
            }
        }
        products.sort(new SortByPrice());
        for (Products flowerpots1 : products) {
            System.out.println(flowerpots1);
        }
        sumProductPrice(products);
        sumProductNumber(products);
    }

    public void chooseDecor() {
        for (int i = 0; i < NUMBER_OF_PRODUCTS; i++) {
            System.out.println("Please choose decor:\n1.Table Flowers\n2.Tape\n3.Wrap Flower\n4.Exit");
            int choiceDecor = scanner.nextInt();
            switch (choiceDecor) {
                case 1: {
                    TablesFlowers tablesFlowers = new TablesFlowers();
                    System.out.println("Enter number of table flowers:");
                    int choiceDecors = scanner.nextInt();
                    tablesFlowers.setNumbers(choiceDecors);
                    tablesFlowers = (TablesFlowers) chooseColors(tablesFlowers);
                    setPriceByAdmin(tablesFlowers);
                    products.add(tablesFlowers);
                    break;
                }
                case 2: {
                    Tape tape = new Tape();
                    System.out.println("Enter number of tape:");
                    int choiceTape = scanner.nextInt();
                    tape.setNumbers(choiceTape);
                    tape = (Tape) chooseColors(tape);
                    setPriceByAdmin(tape);
                    products.add(tape);
                    break;
                }
                case 3: {
                    WrapFlower wrapFlower = new WrapFlower();
                    System.out.println("Enter number of wrap flower:");
                    int choiceWrapFlower = scanner.nextInt();
                    wrapFlower.setNumbers(choiceWrapFlower);
                    wrapFlower = (WrapFlower) chooseColors(wrapFlower);
                    setPriceByAdmin(wrapFlower);
                    products.add(wrapFlower);
                    break;
                }
            }
        }
        products.sort(new SortByPrice());
        for (Products decor : products) {
            System.out.println(decor);
        }
        sumProductPrice(products);
        sumProductNumber(products);
    }

    public Products chooseColors(Products products) {
        System.out.println("Please, choose colors:\n1.White\n2.Red\n3.Yellow\n4.Orange");
        int chooseColors = scanner.nextInt();
        switch (chooseColors) {
            case 1: {
                products.setColor(Colors.WHITE);
                break;
            }
            case 2: {
                products.setColor(Colors.RED);
                break;
            }
            case 3: {
                products.setColor(Colors.YELLOW);
                break;
            }
            case 4: {
                products.setColor(Colors.ORANGE);
                break;
            }
        }
        return products;
    }

    public void sumProductPrice(List<? extends Products> products) {
        int price = 0;
        for (Products product : products) {
            price += product.getTotalPrice();
        }
        System.out.println("Total price of selected products=" + price);
    }

    public void sumProductNumber(List<? extends Products> products) {
        int number = 0;
        for (Products product : products) {
            number += product.getNumbers();
        }
        System.out.println("Total number of selected products=" + number);
    }

}
