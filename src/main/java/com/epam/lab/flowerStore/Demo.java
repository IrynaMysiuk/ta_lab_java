package com.epam.lab.flowerStore;

import com.epam.lab.flowerStore.base.Shop;

import java.util.Scanner;

public class Demo {

    public static void main(String[] args) {

        System.out.println("Please choose products in our shop:\n1.Flowers\n2.Flowerpots\n3.Decor");
        Shop myShop = new Shop();
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        switch (choice) {
            case 1: {
                myShop.chooseFlowers();
                break;
            }
            case 2: {
                myShop.chooseFlowerpots();
                break;
            }
            case 3: {
                myShop.chooseDecor();
                break;
            }
        }
    }
}
