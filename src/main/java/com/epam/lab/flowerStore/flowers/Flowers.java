package com.epam.lab.flowerStore.flowers;

import com.epam.lab.flowerStore.base.Products;

public abstract class Flowers extends Products {


    public static Roses addRoses() {
        return new Roses();
    }

    public static Lilies addLilies() {
        return new Lilies();
    }

    public static Daisies addDaisies() {
        return new Daisies();
    }

}
