package com.epam.lab.flowerStore.flowers;

import com.epam.lab.flowerStore.base.Products;

public class Roses extends Flowers {

    private RoseSort sort;

    public int getPrice() {
        return sort.getPrice();
    }

    @Override
    public Products setPrice(int price) {
        return null;
    }

    public String getSort() {
        return sort.name();
    }

    public void setSort(RoseSort sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "Roses{" +
                "sort='" + getSort() + '\'' +
                ", price=" + getPrice() +
                ", number=" + getNumbers() +
                ", Total result=" + getTotalPrice() + ", color=" + getColor() + '}';
    }
}
