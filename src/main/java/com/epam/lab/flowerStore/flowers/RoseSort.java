package com.epam.lab.flowerStore.flowers;

public enum RoseSort {
    PATIO(30),
    MINI(56),
    SCRABBLE(20),
    TEA(10),
    BUSH(120);
    int price;

    RoseSort(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
