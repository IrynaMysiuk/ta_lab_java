package com.epam.lab.flowerStore.flowers;

import com.epam.lab.flowerStore.base.Products;

import static com.epam.lab.flowerStore.base.Constants.PRICE_DAISIES;

public class Daisies extends Flowers {

    private int price;


    public int getPrice() {
        if (price == 0) {
            price = PRICE_DAISIES;
        }
        return price;
    }


    public Products setPrice(int price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Daisies{" + " price="
                + getPrice() + ", number="
                + getNumbers() + ", Total result="
                + getTotalPrice() + ", color=" + getColor() + '}';
    }
}
