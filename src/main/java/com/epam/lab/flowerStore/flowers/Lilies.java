package com.epam.lab.flowerStore.flowers;

import com.epam.lab.flowerStore.base.Products;

public class Lilies extends Flowers {
    private LiliaSort sort;


    public String getLiliaSort() {
        return sort.name();
    }

    public void setLiliaSort(LiliaSort liliaSort) {
        this.sort = liliaSort;
    }

    @Override
    public int getPrice() {
        return sort.getPrice();
    }

    @Override
    public Products setPrice(int price) {
        return null;
    }


    @Override
    public String toString() {
        return "Lilies{" + "sort='"
                + getLiliaSort() + '\'' +
                ", price=" + getPrice() +
                ", number=" + getNumbers() +
                ", Total result=" + getTotalPrice() + ", color=" + getColor() + '}';
    }
}
