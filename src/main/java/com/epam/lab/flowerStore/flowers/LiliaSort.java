package com.epam.lab.flowerStore.flowers;

public enum LiliaSort {
    ASIAN(30),
    ROYAL(55),
    CURLY(72),
    AMERICAN(23);

    private int price;

    LiliaSort(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
