package com.epam.lab.flowerStore.decors;

import com.epam.lab.flowerStore.base.Products;

import static com.epam.lab.flowerStore.base.Constants.PRICE_TAPE;

public class Tape extends Decors {
    private int price;

    public int getPrice() {

        if (price == 0) {
            price = PRICE_TAPE;
        }
        return price;
    }

    public Products setPrice(int price) {

        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Tape{" +
                '\'' + " price=" + getPrice() +
                ", number=" + getNumbers() +
                ", Total result=" + getTotalPrice() + ", color=" + getColor() + '}';
    }
}
