package com.epam.lab.flowerStore.decors;

import com.epam.lab.flowerStore.base.Constants;
import com.epam.lab.flowerStore.base.Products;

public class TablesFlowers extends Decors implements Constants {
    private int price;

    public int getPrice() {
        if (price == 0) {
            price = PRICE_TABLE_FLOWERS;
        }
        return price;
    }

    public Products setPrice(int price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Table for flowers{" +
                " price=" + getPrice() +
                ", number=" + getNumbers() +
                ", Total result=" + getTotalPrice() + ", color=" + getColor() + '}';
    }
}
