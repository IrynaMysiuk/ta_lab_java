package com.epam.lab.flowerStore.decors;

import com.epam.lab.flowerStore.base.Products;

public abstract class Decors extends Products {


    private int numbers;

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
