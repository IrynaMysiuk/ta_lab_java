package com.epam.lab.flowerStore.decors;

import com.epam.lab.flowerStore.base.Products;

import static com.epam.lab.flowerStore.base.Constants.PRICE_WRAP_FLOWER;

public class WrapFlower extends Decors {
    private int price;

    public int getPrice() {
        if (price == 0) {
            price = PRICE_WRAP_FLOWER;
        }
        return price;
    }

    public Products setPrice(int price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Wrap flower{" +
                '\'' + " price=" + getPrice() +
                ", number=" + getNumbers() +
                ", Total result=" + getTotalPrice() + ", color=" + getColor() + '}';
    }
}
