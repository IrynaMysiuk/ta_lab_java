package com.epam.lab.xml.XMLConvert;

import com.epam.lab.xml.Model.Constants;
import com.epam.lab.xml.Model.Gem;
import com.epam.lab.xml.Model.Gems;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ListObjectsConvert {
    private Gems gems;

    public void sortList(List<Gem> list) {
        list.sort(new NameComparator());
        for (Gem st : list) {
            System.out.println(st.toString());
        }
    }

    public List<Gem> outputResults() {
        List<Gem> listGem = new ArrayList<>();
        try {
            File file = new File(Constants.pathToXML);
            JAXBContext jaxbContext = JAXBContext.newInstance(Gems.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            gems = (Gems) unmarshaller.unmarshal(file);
            List<Gem> list = gems.getGem();
            listGem = list;
            for (Gem gem : list) {
                System.out.println(gem.toString());
            }
            System.out.println();
            list.sort(new NameComparator());
            sortElements();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return listGem;
    }

    private void transformHTML(String outputFileName, Transformer transformer, JAXBSource source) {
        try (OutputStream htmlFile = new FileOutputStream(outputFileName)) {
            transformer.transform(source, new StreamResult(htmlFile));
        } catch (IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private void sortElements() {
        String outputFileName = Constants.pathToSortedHTML;
        TransformerFactory tf = TransformerFactory.newInstance();
        StreamSource xslt = new StreamSource(Constants.pathToXSL);
        Transformer transformer;
        try {
            transformer = tf.newTransformer(xslt);
            JAXBContext jc = JAXBContext.newInstance(Gems.class);
            JAXBSource source = new JAXBSource(jc, gems);
            transformHTML(outputFileName, transformer, source);
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}