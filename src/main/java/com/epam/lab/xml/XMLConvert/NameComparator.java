package com.epam.lab.xml.XMLConvert;

import com.epam.lab.xml.Model.Gem;

import java.util.Comparator;

class NameComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        return ((Gem) o1).getName().compareTo(((Gem) o2).getName());
    }
}