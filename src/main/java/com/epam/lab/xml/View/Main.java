package com.epam.lab.xml.View;

import com.epam.lab.logger.Application;
import com.epam.lab.xml.Model.Gem;
import com.epam.lab.xml.XMLConvert.ConvertToHTML;
import com.epam.lab.xml.XMLConvert.ConvertXML;
import com.epam.lab.xml.XMLConvert.ListObjectsConvert;
import com.epam.lab.xml.XMLParser.XMLParserDOM;
import com.epam.lab.xml.XMLParser.XMLParserSAX;
import com.epam.lab.xml.XMLParser.XMLParserStAX;
import com.epam.lab.xml.XMLValidation.XMLValidation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.TransformerException;
import java.util.List;

public class Main {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws TransformerException {
        logger.info("Task 4");
        XMLParserDOM parserDOM = new XMLParserDOM();
        parserDOM.outputResults();
        XMLParserSAX parserSAX = new XMLParserSAX();
        parserSAX.outputResults();
        XMLParserStAX parserStAX = new XMLParserStAX();
        parserStAX.outputResults();
        logger.info("Task 5");
        XMLValidation xmlValidation = new XMLValidation();
        logger.info(String.format("\ngem.xml validates against gem.xsd? %s\n",
                xmlValidation.validateXMLSchema("xml/gem.xsd", "xml/gem.xml")));
        logger.info("Task 6");
        ConvertToHTML convertToHTML = new ConvertToHTML();
        logger.info(convertToHTML.toString());
        ConvertXML convertXML = new ConvertXML();
        logger.info(convertXML.toString());
        ListObjectsConvert listObjectsConvert = new ListObjectsConvert();
        List<Gem> listGem;
        listGem = listObjectsConvert.outputResults();
        logger.info("Task 7");
        listObjectsConvert.sortList(listGem);
    }
}
