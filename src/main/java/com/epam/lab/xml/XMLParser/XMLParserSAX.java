package com.epam.lab.xml.XMLParser;

import com.epam.lab.logger.Application;
import com.epam.lab.xml.Model.Constants;
import com.epam.lab.xml.Model.Gem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class XMLParserSAX {
    private static Logger logger = LogManager.getLogger(Application.class);

    public void outputResults() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            MyHandler handler = new MyHandler();
            saxParser.parse(new File(Constants.pathToXML), handler);
            List<Gem> empList = handler.getGemList();
            for (Gem gem : empList)
                logger.info(gem);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }
}

