package com.epam.lab.xml.XMLParser;

import com.epam.lab.logger.Application;
import com.epam.lab.xml.Model.Constants;
import com.epam.lab.xml.Model.Gem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLParserStAX {
    private static Logger logger = LogManager.getLogger(Application.class);

    public void outputResults() {
        String fileName = Constants.pathToXML;
        List<Gem> gemList = parseXML(fileName);
        for (Gem emp : gemList) {
            logger.info(emp.toString());
        }
    }

    private List<Gem> parseXML(String fileName) {
        List<Gem> gemList = new ArrayList();
        Gem gem = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try (FileInputStream fis = new FileInputStream(fileName)) {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(fis);
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "gem":
                            gem = new Gem();
                            Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                            if (idAttr != null) {
                                gem.setId(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "parameters":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setParameters(xmlEvent.asCharacters().getData());
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "preciousness":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setPreciousness(xmlEvent.asCharacters().getData());
                            break;
                        case "yearOfProduction":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setYearOfProduction(xmlEvent.asCharacters().getData());
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setValue(xmlEvent.asCharacters().getData());
                            break;
                        case "valuable":
                            xmlEvent = xmlEventReader.nextEvent();
                            gem.setValuable(xmlEvent.asCharacters().getData());
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("gem")) {
                        gemList.add(gem);
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
        return gemList;
    }
}