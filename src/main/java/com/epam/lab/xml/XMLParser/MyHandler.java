package com.epam.lab.xml.XMLParser;

import com.epam.lab.xml.Model.Gem;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MyHandler extends DefaultHandler {

    boolean bName;
    boolean bPreciousness;
    boolean bOrigin;
    boolean bParameters;
    boolean bValue;
    boolean bYearOfProduction;
    boolean bValuable;
    private List<Gem> gemList;
    private Gem gem;

    public List<Gem> getGemList() {
        return gemList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("Gem")) {
            String id = attributes.getValue("id");
            gem = new Gem();
            gem.setId(Integer.parseInt(id));
            if (gemList == null)
                gemList = new ArrayList();
        } else if (qName.equalsIgnoreCase("name")) {
            bName = true;
        } else if (qName.equalsIgnoreCase("preciousness")) {
            bPreciousness = true;
        } else if (qName.equalsIgnoreCase("origin")) {
            bOrigin = true;
        } else if (qName.equalsIgnoreCase("parameters")) {
            bParameters = true;
        } else if (qName.equalsIgnoreCase("value")) {
            bValue = true;
        } else if (qName.equalsIgnoreCase("yearOfProduction")) {
            bYearOfProduction = true;
        } else if (qName.equalsIgnoreCase("valuable")) {
            bValuable = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("Gem")) {
            gemList.add(gem);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        if (bName) {
            gem.setName(new String(ch, start, length));
            bName = false;
        } else if (bPreciousness) {
            gem.setParameters(new String(ch, start, length));
            bPreciousness = false;
        } else if (bOrigin) {
            gem.setOrigin(new String(ch, start, length));
            bOrigin = false;
        } else if (bParameters) {
            gem.setOrigin(new String(ch, start, length));
            bParameters = false;
        } else if (bValue) {
            gem.setValue(new String(ch, start, length));
            bValue = false;
        } else if (bYearOfProduction) {
            gem.setYearOfProduction(new String(ch, start, length));
            bYearOfProduction = false;
        } else if (bValuable) {
            gem.setValuable(new String(ch, start, length));
            bValuable = false;
        }
    }
}