package com.epam.lab.xml.XMLParser;


import com.epam.lab.logger.Application;
import com.epam.lab.xml.Model.Constants;
import com.epam.lab.xml.Model.Gem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class XMLParserDOM {
    private static Logger logger = LogManager.getLogger(Application.class);

    private void showList(Document doc) {
        NodeList nodeList = doc.getElementsByTagName("gem");
        List<Gem> empList = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            empList.add(getOldCard(nodeList.item(i)));
        }
        for (Gem gem : empList) {
            logger.info(gem.toString());
        }
    }

    public void outputResults() {
        String xmlFile = Constants.pathToXML;
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            logger.info("Root element :" + doc.getDocumentElement().getNodeName());
            showList(doc);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private static Gem getOldCard(Node node) {
        Gem gem = new Gem();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            gem.setId(Integer.parseInt(element.getAttribute("id")));
            gem.setName(getTagValue("name", element));
            gem.setParameters(getTagValue("preciousness", element));
            gem.setOrigin(getTagValue("origin", element));
            gem.setOrigin(getTagValue("parameters", element));
            gem.setValue(getTagValue("value", element));
            gem.setYearOfProduction(getTagValue("yearOfProduction", element));
            gem.setValuable(getTagValue("valuable", element));
        }
        return gem;
    }

    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }
}