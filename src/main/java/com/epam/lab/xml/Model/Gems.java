package com.epam.lab.xml.Model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Gems {
    private List<Gem> gems;

    @XmlElement
    public List<Gem> getGem() {
        return gems;
    }

    public void setGem(List<Gem> gems) {
        this.gems = gems;
    }
}