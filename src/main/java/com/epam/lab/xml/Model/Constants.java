package com.epam.lab.xml.Model;

public class Constants {
    public static String pathToXML = "xml/gem.xml";
    public static String pathToXSL = "xml/gem.xsl";
    public static String pathToSortedHTML = "xml/sorted_gem.html";
    public static String pathToHTML = "xml/gem.html";
    public static String newPathToXML = "xml/new_gem.xml";
    public static String newPathToXSL = "xml/changeRootElement.xsl";
    public static String pathToXSLSort = "xml/gem_with_sort.xsl";

    private Constants() {
    }
}

