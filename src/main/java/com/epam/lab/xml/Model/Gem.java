package com.epam.lab.xml.Model;

public class Gem {
    private int id;
    private String name;
    private String preciousness;
    private String origin;
    private String parameters;
    private String yearOfProduction;
    private String value;
    private String valuable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setYearOfProduction(String yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setValuable(String valuable) {
        this.valuable = valuable;
    }

    public String getName() {
        return this.name;
    }

    public String getPreciousness() {
        return this.preciousness;
    }

    public String getParameters() {
        return this.parameters;
    }

    public String getOrigin() {
        return this.origin;
    }

    public String getYearOfProduction() {
        return this.yearOfProduction;
    }

    public String getValue() {
        return this.value;
    }

    public String getValuable() {
        return this.valuable;
    }

    @Override
    public String toString() {
        return String.format("Gem:: ID = %s, Name = %s, Preciousness = %s,Status = %s,Country = %s," +
                        "Year = %s,Author = %s,Valuable = %s;",
                this.id, this.name, this.preciousness, this.parameters, this.origin, this.yearOfProduction,
                this.value, this.valuable);
    }
}
