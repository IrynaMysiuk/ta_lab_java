package com.epam.lab.game;


import com.epam.lab.game.jobs.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.epam.lab.game.Constants.*;
import static com.epam.lab.game.Lottery.getRandomNumberInRange;
import static com.epam.lab.game.console.Game.startGame;


public class Person {

    private List<Jobs> jobs;
    Scanner scanner;

    public Person() {
        jobs = new ArrayList<>();
        scanner = new Scanner(System.in);
    }

    private void getTotalProfit() {
        int sumProfit = 0;
        for (Jobs job : jobs) {
            sumProfit += job.getSalary();
        }
        System.out.println("Your total profit is " + sumProfit);
    }

    private void earnMoney(int score, Jobs job) {
        startGame(score);
        getRandomNumberInRange(job);
        jobs.add(job);
    }

    public void chooseJobs() {
        System.out.println("Please choose jobs:\n1.ATBSeller\n2.Earner\n3.Exit");
        int choiceJobs = scanner.nextInt();
        switch (choiceJobs) {
            case 1: {
                ATBSeller atbSeller = new ATBSeller();
                atbSeller.setSalary();
                System.out.println("You'll earn: " + SALARY_ATB_SELLER);
                earnMoney(START_SCORE, atbSeller);
                break;
            }
            case 2: {
                Earner earner = new Earner();
                earner.setSalary();
                System.out.println("You'll earn: " + SALARY_EARNER);
                earnMoney(START_SCORE, earner);
                break;
            }
        }
        getTotalProfit();
        chooseBusiness();
    }

    public void chooseBusiness() {
        System.out.println("You winner in this level, move in on the next steps!");
        System.out.println("Please choose your business:\n1.Apiarist\n2.Confectioner\n3.Actor");
        int choiceBusiness = scanner.nextInt();
        switch (choiceBusiness) {
            case 1: {
                Apiarist apiarist = new Apiarist();
                apiarist.setSalary();
                System.out.println("You'll earn: " + SALARY_APIARIST);
                earnMoney(BUSINESS_SCORE, apiarist);
                break;
            }
            case 2: {
                Сonfectioner confectioner = new Сonfectioner();
                confectioner.setSalary();
                System.out.println("You'll earn: " + SALARY_CONFECTIONER);
                earnMoney(BUSINESS_SCORE, confectioner);
                break;
            }
            case 3: {
                Actor actor = new Actor();
                actor.setSalary();
                System.out.println("You'll earn: " + SALARY_ACTOR);
                earnMoney(BUSINESS_SCORE, actor);
                break;
            }
        }
        getTotalProfit();
        choosePresident();
    }

    public void choosePresident() {
        President president = new President();
        president.setSalary();
        System.out.println("You'll earn: " + SALARY_PRESIDENT);
        earnMoney(FINAL_SCORE, president);
        getTotalProfit();
        System.out.println("You winner!\nGAME OVER");
        System.out.println("\nCool, you are very persistent person, so you can become president!");
    }
}

