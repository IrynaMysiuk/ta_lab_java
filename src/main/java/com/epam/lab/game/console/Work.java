package com.epam.lab.game.console;

public class Work {

    private Node head;
    private Node tempValue;
    private int size;
    private int score;

    public Work() {
        head = null;
        size = 1;
        score = 0;
    }

    public Node getHead() {
        return head;
    }

    public int getScore() {
        return this.score;
    }

    public void sizeUp() {
        this.size += 1;
        this.score = (this.score * (this.size / 2)) + 10;
    }

    public void gameOver() {
        int sc = this.score;
        System.out.println("--------------------");
        System.out.println("     SUCCESS!     ");
        System.out.println("--------------------");
        System.out.println("You finished this work with a score of: " + sc + "!");
    }

    public void enqueue(int x, int y) {
        Node tmp = new Node(x, y);

        if (head == null) {
            head = tempValue = tmp;
        } else {
            tmp.next = tempValue;
            tempValue = tmp;
        }
    }

    public void move(int x, int y) {
        Node tmp = new Node(x, y);

        if (head == null) {
            head = tempValue = tmp;
        } else {
            head.next = tmp;
            head = tmp;
        }
    }

    public void dequeue(Border game) {
        game.replace(tempValue.getX(), tempValue.getY(), ' ');
        tempValue = tempValue.next;
    }

    public void render(Border game) {
        if (head.getX() == 0 || head.getX() == game.getWidth() - 1)
            gameOver();

        else if (head.getY() == 0 || head.getY() == game.getHeight() - 1)
            gameOver();

        if (size == 1) {
            game.work(head.getX(), head.getY());
        } else {
            Node tmp = tempValue;
            game.work(tmp.getX(), tmp.getY());
        }
    }


    public void right(Border game, int prevX, int prevY) {
        int x = (prevX + 1);
        move(x, prevY);
        dequeue(game);
        render(game);
    }

    public void left(Border game, int prevX, int prevY) {
        int x = (prevX - 1);
        move(x, prevY);
        dequeue(game);
        render(game);
    }

    public void up(Border game, int prevX, int prevY) {
        int y = (prevY - 1);
        move(prevX, y);
        dequeue(game);
        render(game);
    }

    public void down(Border game, int prevX, int prevY) {
        int y = (prevY + 1);
        move(prevX, y);
        dequeue(game);
        render(game);
    }
}
