package com.epam.lab.game.console;

import java.util.Scanner;

import static com.epam.lab.game.console.GameConstants.HEIGHT;
import static com.epam.lab.game.console.GameConstants.WIDTH;

public class Game {

    private static Scanner scanner = new Scanner(System.in);
    private static boolean run = false;

    private static void start() {
        System.out.println("--------------------");
        System.out.println("You are the 'X', the task is the 'O'.\n " +
                "Run into the wall or get min score and it's game over. " +
                "\nPRESS ENTER after EVERY input to move.\n ");
        System.out.println("W = up");
        System.out.println("A = left");
        System.out.println("S = down");
        System.out.println("D = right");
        run = true;
    }

    public static void startGame(int maxScore) {
        Border area = new Border(WIDTH, HEIGHT);
        Work s = new Work();
        Task f = new Task();

        f.randomizer(WIDTH - 1, HEIGHT - 1);
        s.enqueue(WIDTH / 4, HEIGHT / 4);
        area.border();
        area.work(WIDTH / 4, HEIGHT / 4);
        area.task(f.getX(), f.getY());

        start();

        while (run) {
            area.render(s);
            if ((s.getHead().getX() == f.getX()) && (s.getHead().getY() == f.getY())) {
                s.sizeUp();
                f.randomizer(WIDTH - 1, HEIGHT - 1);

                area.task(f.getX(), f.getY());
            }
            System.out.print("Input direction > ");
            try {
                char input;
                switch (input = scanner.nextLine().charAt(0)) {
                    case 'a':
                        s.left(area, s.getHead().getX(), s.getHead().getY());
                        break;

                    case 's':
                        s.down(area, s.getHead().getX(), s.getHead().getY());
                        break;

                    case 'd':
                        s.right(area, s.getHead().getX(), s.getHead().getY());
                        break;

                    case 'w':
                        s.up(area, s.getHead().getX(), s.getHead().getY());
                        break;
                }
                if (s.getScore() >= maxScore) {
                    s.gameOver();
                    break;
                }
            } catch (StringIndexOutOfBoundsException e) {
                System.out.println("-------ERROR!-------");
                System.out.println("Press W,A,S, or D");
                System.out.println("--------------------");
            }
        }
    }
}
