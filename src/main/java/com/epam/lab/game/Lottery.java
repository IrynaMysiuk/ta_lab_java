package com.epam.lab.game;

import com.epam.lab.game.jobs.Jobs;

import java.util.Scanner;

public class Lottery {

    public static void getRandomNumberInRange(Jobs job) {

        Scanner keyboard = new Scanner(System.in);
        int count = 0;
        int a = 1 + (int) (Math.random() * 3);
        int guess = 0;

        System.out.println("I am thinking of a number from 1 to 3"
                + " ... guess what it is ?");

        while (guess != a) {
            guess = keyboard.nextInt();
            count++;
            if (guess > a) {
                System.out.println("lower!");
            } else if (guess < a) {
                System.out.println("higher!");
            }
        }
        System.out.println("Congratulations. You guessed the number with "
                + count + " tries! This number is = " + guess + "\n");

        if (count == 1) {
            job.setSalary(job.getSalary() * 2);
            System.out.println("Congratulations. You guess in the first time "
                    + count + " tries! This number is = " + guess + "\n So your salary doubles = " + job.getSalary() + "\n");
        }
    }

}




