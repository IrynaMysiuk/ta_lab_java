package com.epam.lab.game.jobs;

import static com.epam.lab.game.Constants.SALARY_CONFECTIONER;

public class Сonfectioner extends Jobs {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary() {
        this.salary = SALARY_CONFECTIONER;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Confectioner{" + " Salary="
                + getSalary() + ", Number month="
                + getPerMonth() + ", Total salary="
                + getTotalPrice() + ", Bonus=" + "\n}";
    }
}
