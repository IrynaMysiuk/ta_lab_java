package com.epam.lab.game.jobs;

import static com.epam.lab.game.Constants.SALARY_PRESIDENT;

public class President extends Jobs {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary() {
        this.salary = SALARY_PRESIDENT;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "President{" + " Salary="
                + getSalary() + ", Number month="
                + getPerMonth() + ", Total salary="
                + getTotalPrice() + ", Bonus=" + "\n}";
    }
}
