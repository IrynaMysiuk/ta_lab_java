package com.epam.lab.game.jobs;

import static com.epam.lab.game.Constants.SALARY_EARNER;

public class Earner extends Jobs {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary() {
        this.salary = SALARY_EARNER;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Earner{" + " Salary="
                + getSalary() + ", Number month="
                + getPerMonth() + ", Total salary="
                + getTotalPrice() + ", Bonus=" + "\n";
    }
}
