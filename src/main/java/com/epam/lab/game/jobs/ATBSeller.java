package com.epam.lab.game.jobs;

import static com.epam.lab.game.Constants.SALARY_ATB_SELLER;

public class ATBSeller extends Jobs {

    private int salary;


    public int getSalary() {
        return salary;
    }

    public void setSalary() {
        this.salary = SALARY_ATB_SELLER;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "ATBSeller{" + " Salary="
                + getSalary() + ", Number month="
                + getPerMonth() + ", Total salary="
                + getTotalPrice() + ", Bonus=" + "\n";
    }
}
