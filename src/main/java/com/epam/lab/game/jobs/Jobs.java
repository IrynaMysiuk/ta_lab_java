package com.epam.lab.game.jobs;


public abstract class Jobs {
    private int months;
    protected int totalPrice;

    public abstract int getSalary();

    public abstract void setSalary(int salary);

    public int getPerMonth() {
        return months;
    }

    public void setPerMonth(int months) {
        this.months = months;
        System.out.println("Enter how much month you work:\n" + months);
    }

    public int getTotalPrice() {
        return getPerMonth() * getSalary();
    }

    public void getRandomNumberInRange() {
        return;
    }
}
