package com.epam.lab.game;

public interface Constants {
    public static final int SALARY_ATB_SELLER = 3000;
    public static final int SALARY_EARNER = 8000;
    public static final int SALARY_APIARIST = 26000;
    public static final int SALARY_CONFECTIONER = 38000;
    public static final int SALARY_ACTOR = 54000;
    public static final int SALARY_PRESIDENT = 100000;

    public static final int START_SCORE = 30;
    public static final int BUSINESS_SCORE = 60;
    public static final int FINAL_SCORE = 100;
}
