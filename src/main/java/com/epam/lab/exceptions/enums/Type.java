package com.epam.lab.exceptions.enums;

public enum Type {
    TREE,
    BUSH,
    GRASS,
    FLOWER
}
