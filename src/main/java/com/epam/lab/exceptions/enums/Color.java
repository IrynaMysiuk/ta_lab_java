package com.epam.lab.exceptions.enums;

public enum Color {
    RED,
    WHITE,
    PURPLE,
    PINK,
    YELLOW,
    BLACK,
    GREEN,
    ORANGE
}
