package com.epam.lab.exceptions;

import com.epam.lab.exceptions.enums.Color;
import com.epam.lab.exceptions.enums.Type;
import com.epam.lab.exceptions.plantException.ColorException;
import com.epam.lab.exceptions.plantException.TypeException;

public class Plants {
    private int size;
    private Color color;
    private Type type;

    private Plants(int size, String color, String type) throws ColorException, TypeException {
        this.size = size;
        try {
            this.color = Color.valueOf(color);
        } catch (IllegalArgumentException e) {
            throw new ColorException(e);
        }
        try {
            this.type = Type.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new TypeException(e);
        }


    }

    private static Plants newPlant(int size, String color, String type) {
        Plants plant = null;
        try {
            plant = new Plants(size, color, type);
        } catch (ColorException | TypeException e) {
            e.printStackTrace();
        }
        return plant;
    }

    public static void main(String[] args) {
        Plants[] plants = {
                newPlant(1, "GREEN", "BUSH"),
                newPlant(2, "dark", "kvitka"),
                newPlant(3, "332", "211"),
                newPlant(4, "PINK", "tree"),
                newPlant(5, "blue", "red"),
        };
        for (Plants plant : plants) {
            System.out.println(plant);
        }
    }

    @Override
    public String toString() {
        return "Plants{" +
                "size=" + size +
                ", color=" + color +
                ", type=" + type +
                '}';
    }
}
