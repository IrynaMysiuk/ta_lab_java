package com.epam.lab.exceptions.plantException;

public class TypeException extends Exception {
    public TypeException(Throwable exception) {
        super("Non-existed type " + exception);
    }
}
