package com.epam.lab.exceptions.plantException;

public class ColorException extends Exception {
    public ColorException(Throwable exception) {
        super("Non-existed color " + exception);
    }
}
