package com.epam.lab.exceptions;


import java.util.Scanner;

public class RectangleException {
    static Scanner input = new Scanner(System.in);

    public static void checkNonNumericValue(String value) {
        if (!value.matches("\\d+")) {
            throw new NumberFormatException("This data type is incorrect. Please input integer value!");
        }
    }

    public static int squareRectangle(int a, int b) {
        if (a < 0 || b < 0) {
            throw new ArithmeticException("You entered negative value. You should input positive value!");
        }
        return a * b;
    }

    public static void main(String[] args) {
        System.out.println("Enter your width (a): ");
        String width = input.nextLine();
        checkNonNumericValue(width);
        System.out.println("Enter your height (b): ");
        String height = input.nextLine();
        checkNonNumericValue(height);
        System.out.println(
                "The rectangle has a width of " + width
                        + " and a height of " + height);

        System.out.println("the area is " + squareRectangle(Integer.parseInt(width), Integer.parseInt(height)));

    }
};


