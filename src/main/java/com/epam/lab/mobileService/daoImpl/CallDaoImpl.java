package com.epam.lab.mobileService.daoImpl;

import com.epam.lab.logger.Application;
import com.epam.lab.mobileService.core.SingletonDBConnection;
import com.epam.lab.mobileService.entities.Call;
import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CallDaoImpl extends EntityDaoImpl {
    //Open-Closed all class DaoImpl
    private static Logger logger = LogManager.getLogger(Application.class);

    public String getDataBaseName() {
        return Call.class.getAnnotation(DBTable.class).value();
    }

    public void createCallTable() {
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            String createTableCmd = String.format("CREATE TABLE IF NOT EXISTS %s" +
                            "(id_call int primary key unique auto_increment," +
                            " id_sender int, id_receiver int, durationCall time, FOREIGN KEY (id_sender)" +
                            " REFERENCES clients(id_client))",
                    getDataBaseName());
            statement.execute(createTableCmd);
            logger.info(createTableCmd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(Call call) {
        Connection connection = SingletonDBConnection.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(String.format("INSERT INTO %s " +
                "VALUES (?, ?, ?, ?)", getDataBaseName()))) {
            preparedStatement.setInt(1, call.getIdCall());
            preparedStatement.setInt(2, call.getIdSender());
            preparedStatement.setInt(3, call.getIdReceiver());
            preparedStatement.setTime(4, call.getDurationCall());
            preparedStatement.executeUpdate();
            logger.info(String.format("INSERT INTO %s VALUES (%s, %s, %s, %s)",
                    getDataBaseName(), call.getIdCall(), call.getIdSender(),
                    call.getIdReceiver(), call.getDurationCall()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Call selectById(int idCall) {
        Connection connection = SingletonDBConnection.getConnection();
        Call call = new Call();
        String selectByIdCmd = String.format("SELECT * FROM %s WHERE id_call = ?", getDataBaseName());
        try (PreparedStatement preparedStatement = connection
                .prepareStatement(selectByIdCmd)) {
            logger.info(selectByIdCmd.replaceAll("\\?", String.valueOf(idCall)));
            preparedStatement.setInt(1, idCall);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                call = convertCallOutput(resultSet);
            }
            logger.info(call.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return call;
    }

    public List<Call> selectAll() {
        List<Call> calls = new ArrayList<>();
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet;
            String selectAllCmd = String.format("SELECT * FROM %s", getDataBaseName());
            resultSet = statement.executeQuery(selectAllCmd);
            logger.info(selectAllCmd);
            while (resultSet.next()) {
                Call call = convertCallOutput(resultSet);
                calls.add(call);
                logger.info(call.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calls;
    }

    private Call convertCallOutput(ResultSet resultSet) throws SQLException {
        Call call = new Call();
        List<String> dbFields = getDBFields(call);
        call.setIdCall(resultSet.getInt(dbFields.get(0)))
                .setIdSender(resultSet.getInt(dbFields.get(1)))
                .setIdReceiver(resultSet.getInt(dbFields.get(2)))
                .setDurationCall(resultSet.getTime(dbFields.get(3)));
        return call;
    }

    public void deleteById(int callId) throws NoSuchFieldException {
        Connection connection = SingletonDBConnection.getConnection();
        String deleteById = String.format("DELETE FROM %s WHERE %s = ?",
                getDataBaseName(),
                Call.class.getDeclaredField("idCall").getAnnotation(DB_Field.class).value());
        try (PreparedStatement preparedStatement = connection.prepareStatement(deleteById)) {
            preparedStatement.setInt(1, callId);
            preparedStatement.executeUpdate();
            logger.info(deleteById.replaceAll("\\?", String.valueOf(callId)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
