package com.epam.lab.mobileService.daoImpl;

import com.epam.lab.mobileService.entities.annotations.DB_Field;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public abstract class EntityDaoImpl {
// Dependency Inversion
    public abstract String getDataBaseName();

    public abstract Object selectById(int id);

    public abstract List<?> selectAll();

    public abstract void deleteById(int id) throws NoSuchFieldException;

    public List<String> getDBFields(Object object) {
        List<String> dbFields = new ArrayList<>();
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            dbFields.add(field.getAnnotation(DB_Field.class).value());
            field.setAccessible(false);
        }
        return dbFields;
    }
}
