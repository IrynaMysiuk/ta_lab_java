package com.epam.lab.mobileService.daoImpl;

import com.epam.lab.logger.Application;
import com.epam.lab.mobileService.core.SingletonDBConnection;
import com.epam.lab.mobileService.entities.MobileClient;
import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoImpl extends EntityDaoImpl {
    public static final String QUESTION_MARK = "?";
    private static Logger logger = LogManager.getLogger(Application.class);

    public String getDataBaseName() {
        return MobileClient.class.getAnnotation(DBTable.class).value();
    }

    public void createClientTable() {
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            String createTableCmd = String.format(" CREATE TABLE IF NOT EXISTS %s (id_client int primary key unique auto_increment,lastName varchar(55), firstName varchar(55), " +
                            "birthDate date, phoneNumber int, id_tariff int, money double, foreign key (id_tariff) references tariffs(id_tariff));",
                    getDataBaseName());
            statement.execute(createTableCmd);
            logger.info(createTableCmd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(MobileClient mobileClient) {
        Connection connection = SingletonDBConnection.getConnection();
        String insertCmd = String.format("INSERT INTO %s VALUES (value, value, value, value, value, value, value)",
                getDataBaseName());
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                insertCmd.replaceAll("value", QUESTION_MARK))) {
            preparedStatement.setInt(1, mobileClient.getId());
            preparedStatement.setString(2, mobileClient.getLastName());
            preparedStatement.setString(3, mobileClient.getFirstName());
            preparedStatement.setDate(4, mobileClient.getCalls());
            preparedStatement.setInt(5, mobileClient.getMessage());
            preparedStatement.setInt(6, mobileClient.getIdTariff());
            preparedStatement.setInt(7, mobileClient.getMoney());
            preparedStatement.executeUpdate();
            logger.info(String.format(insertCmd.replaceAll("value", "%s"),
                    mobileClient.getId(), mobileClient.getLastName(), mobileClient.getFirstName(),
                    mobileClient.getCalls(), mobileClient.getMessage(), mobileClient.getIdTariff(), mobileClient.getMoney()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public MobileClient selectById(int idClient) {
        Connection connection = SingletonDBConnection.getConnection();
        MobileClient client = new MobileClient();
        String selectByIdCmd = String.format("SELECT * FROM %s WHERE id_client = ?", getDataBaseName());
        try (PreparedStatement preparedStatement = connection.prepareStatement(selectByIdCmd)) {
            logger.info(selectByIdCmd.replaceAll("\\?", String.valueOf(idClient)));
            preparedStatement.setInt(1, idClient);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                client = convertClientOutput(resultSet);
            }
            logger.info(client.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    public MobileClient selectByPhoneNumber(int phoneNumber) {
        Connection connection = SingletonDBConnection.getConnection();
        MobileClient client = new MobileClient();
        String selectByIdCmd = String.format("SELECT * FROM %s WHERE phoneNumber = ?", getDataBaseName());
        try (PreparedStatement preparedStatement = connection.prepareStatement(selectByIdCmd)) {
            logger.info(selectByIdCmd.replaceAll("\\?", String.valueOf(phoneNumber)));
            preparedStatement.setInt(1, phoneNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                client = convertClientOutput(resultSet);
            }
            if (client.getId() == 0) {
                throw new RuntimeException("Incorrect phone number!");
            }
            logger.info(client.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    public List<MobileClient> selectAll() {
        List<MobileClient> clients = new ArrayList<>();
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet;
            String selectAllCmd = String.format("SELECT * FROM %s", getDataBaseName());
            resultSet = statement.executeQuery(selectAllCmd);
            logger.info(selectAllCmd);
            while (resultSet.next()) {
                MobileClient client = convertClientOutput(resultSet);
                clients.add(client);
                logger.info(client.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clients;
    }

    private MobileClient convertClientOutput(ResultSet resultSet) throws SQLException {
        MobileClient client = new MobileClient();
        List<String> dbFields = getDBFields(client);
        client.setId(resultSet.getInt(dbFields.get(0)))
                .setLastName(resultSet.getString(dbFields.get(1)))
                .setFirstName(resultSet.getString(dbFields.get(2)))
                .setBirthDate(resultSet.getDate(dbFields.get(3)))
                .setPhoneNumber(resultSet.getInt(dbFields.get(4)))
                .setIdTariff(resultSet.getInt(dbFields.get(5)))
                .setMoney(resultSet.getInt(dbFields.get(6)));
        return client;
    }

    public void deleteById(int id) throws NoSuchFieldException {
        Connection connection = SingletonDBConnection.getConnection();
        String deleteById = String.format("DELETE FROM %s WHERE %s = ?",
                getDataBaseName(),
                MobileClient.class.getDeclaredField("idClient").getAnnotation(DB_Field.class).value());
        try (PreparedStatement preparedStatement = connection.prepareStatement(deleteById)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            logger.info(deleteById.replaceAll("\\?", String.valueOf(id)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(MobileClient client) {
        Connection connection = SingletonDBConnection.getConnection();
        String updateClient = "UPDATE %s SET money = %s WHERE id_client = %s";
        try (PreparedStatement preparedStatement = connection.prepareStatement(String.format(updateClient,
                getDataBaseName(), QUESTION_MARK, QUESTION_MARK))) {
            preparedStatement.setDouble(1, client.getMoney());
            preparedStatement.setInt(2, client.getId());
            preparedStatement.executeUpdate();
            logger.info(String.format(updateClient, getDataBaseName(), client.getMoney(), client.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
