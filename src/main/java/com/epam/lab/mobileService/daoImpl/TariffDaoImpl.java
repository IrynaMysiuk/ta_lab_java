package com.epam.lab.mobileService.daoImpl;

import com.epam.lab.logger.Application;
import com.epam.lab.mobileService.core.SingletonDBConnection;
import com.epam.lab.mobileService.entities.Tariff;
import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TariffDaoImpl extends EntityDaoImpl {
    private static Logger logger = LogManager.getLogger(Application.class);

    public String getDataBaseName() {
        return Tariff.class.getAnnotation(DBTable.class).value();
    }

    public void createTariffTable() {
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            String createTableCmd = String.format("CREATE TABLE IF NOT EXISTS %s (id_tariff int primary key unique auto_increment," +
                            "price int, internet boolean, calls boolean, message boolean);",
                    getDataBaseName());
            statement.execute(createTableCmd);
            logger.info(createTableCmd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(Tariff tariff) {
        Connection connection = SingletonDBConnection.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(String.format("INSERT INTO %s " +
                "VALUES (?, ?, ?, ?, ?)", getDataBaseName()))) {
            preparedStatement.setInt(1, tariff.getIdTariff());
            preparedStatement.setInt(2, tariff.getPrice());
            preparedStatement.setBoolean(3, tariff.getInternet());
            preparedStatement.setBoolean(4, tariff.getCalls());
            preparedStatement.setBoolean(5, tariff.getMessage());
            preparedStatement.executeUpdate();
            logger.info(String.format("INSERT INTO %s VALUES (%s, %s, %s, %s, %s)",
                    getDataBaseName(), tariff.getId(), tariff.getPrice(), tariff.getInternet(),
                    tariff.getCalls(), tariff.getMessage()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Tariff selectById(int id_tariff) {
        Connection connection = SingletonDBConnection.getConnection();
        Tariff tariff = new Tariff();
        String selectByIdCmd = String.format("SELECT * FROM %s WHERE id_tariff = ?", getDataBaseName());
        try (PreparedStatement preparedStatement = connection
                .prepareStatement(selectByIdCmd)) {
            logger.info(selectByIdCmd.replaceAll("\\?", String.valueOf(id_tariff)));
            preparedStatement.setInt(1, id_tariff);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                tariff = convertTariffOutput(resultSet);
            }
            logger.info(tariff.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tariff;
    }

    public List<Tariff> selectAll() {
        List<Tariff> tariffs = new ArrayList<>();
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet;
            String selectAllCmd = String.format("SELECT * FROM %s", getDataBaseName());
            resultSet = statement.executeQuery(selectAllCmd);
            logger.info(selectAllCmd);
            while (resultSet.next()) {
                Tariff tariff = convertTariffOutput(resultSet);
                tariffs.add(tariff);
                logger.info(tariff.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tariffs;
    }

    private Tariff convertTariffOutput(ResultSet resultSet) throws SQLException {
        Tariff tariff = new Tariff();
        List<String> dbFields = getDBFields(tariff);
        tariff.setIdTariff(resultSet.getInt(dbFields.get(0)))
                .setPrice(resultSet.getInt(dbFields.get(1)))
                .setInternet(resultSet.getBoolean(dbFields.get(2)))
                .setCalls(resultSet.getBoolean(dbFields.get(3)))
                .setMessage(resultSet.getBoolean(dbFields.get(4)));
        return tariff;
    }

    public void deleteById(int idTariff) throws NoSuchFieldException {
        Connection connection = SingletonDBConnection.getConnection();
        String deleteById = String.format("DELETE FROM %s WHERE %s = ?",
                getDataBaseName(),
                Tariff.class.getDeclaredField("idTariff").getAnnotation(DB_Field.class).value());
        try (PreparedStatement preparedStatement = connection.prepareStatement(deleteById)) {
            preparedStatement.setInt(1, idTariff);
            preparedStatement.executeUpdate();
            logger.info(deleteById.replaceAll("\\?", String.valueOf(idTariff)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
