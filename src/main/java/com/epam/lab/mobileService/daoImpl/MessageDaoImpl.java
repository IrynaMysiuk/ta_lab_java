package com.epam.lab.mobileService.daoImpl;

import com.epam.lab.logger.Application;
import com.epam.lab.mobileService.core.SingletonDBConnection;
import com.epam.lab.mobileService.entities.Message;
import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDaoImpl extends EntityDaoImpl {
    private static Logger logger = LogManager.getLogger(Application.class);

    public String getDataBaseName() {
        return Message.class.getAnnotation(DBTable.class).value();
    }

    public void createMessageTable() {
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            String createTableCmd = String.format("CREATE TABLE IF NOT EXISTS %s " +
                            "(id_message int primary key unique auto_increment, " +
                            "id_sender int, id_receiver int, textMessage varchar(255)," +
                            " timeMessage time,FOREIGN KEY (id_sender) REFERENCES clients(id_client))",
                    getDataBaseName());
            statement.execute(createTableCmd);
            logger.info(createTableCmd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(Message message) {
        Connection connection = SingletonDBConnection.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(String.format("INSERT INTO %s " +
                "VALUES (?, ?, ?, ?, ?)", getDataBaseName()))) {
            preparedStatement.setInt(1, message.getIdMessage());
            preparedStatement.setInt(2, message.getIdSender());
            preparedStatement.setInt(3, message.getIdReceiver());
            preparedStatement.setString(4, message.getTextMessage());
            preparedStatement.setTime(5, message.getTimeMessage());
            preparedStatement.executeUpdate();
            logger.info(String.format("INSERT INTO %s VALUES (%s, %s, %s, %s, %s)",
                    getDataBaseName(), message.getIdMessage(), message.getIdSender(), message.getIdReceiver(),
                    message.getTextMessage(), message.getTimeMessage()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Message selectById(int idMessage) {
        Connection connection = SingletonDBConnection.getConnection();
        Message message = new Message();
        String selectByIdCmd = String.format("SELECT * FROM %s WHERE id_message = ?", getDataBaseName());
        try (PreparedStatement preparedStatement = connection
                .prepareStatement(selectByIdCmd)) {
            logger.info(selectByIdCmd.replaceAll("\\?", String.valueOf(idMessage)));
            preparedStatement.setInt(1, idMessage);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                message = convertMessageOutput(resultSet);
            }
            logger.info(message.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public List<Message> selectAll() {
        List<Message> messages = new ArrayList<>();
        Connection connection = SingletonDBConnection.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet;
            String selectAllCmd = String.format("SELECT * FROM %s", getDataBaseName());
            resultSet = statement.executeQuery(selectAllCmd);
            logger.info(selectAllCmd);
            while (resultSet.next()) {
                Message message = convertMessageOutput(resultSet);
                messages.add(message);
                logger.info(message.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messages;
    }

    private Message convertMessageOutput(ResultSet resultSet) throws SQLException {
        Message message = new Message();
        List<String> dbFields = getDBFields(message);
        message.setIdMessage(resultSet.getInt(dbFields.get(0)))
                .setIdSender(resultSet.getInt(dbFields.get(1)))
                .setIdReceiver(resultSet.getInt(dbFields.get(2)))
                .setTextMessage(resultSet.getString(dbFields.get(3)))
                .setTimeMessage(resultSet.getTime(dbFields.get(4)));
        return message;
    }

    public void deleteById(int messageId) throws NoSuchFieldException {
        Connection connection = SingletonDBConnection.getConnection();
        String deleteById = String.format("DELETE FROM %s WHERE %s = ?",
                getDataBaseName(),
                Message.class.getDeclaredField("idMessage").getAnnotation(DB_Field.class).value());
        try (PreparedStatement preparedStatement = connection.prepareStatement(deleteById)) {
            preparedStatement.setInt(1, messageId);
            preparedStatement.executeUpdate();
            logger.info(deleteById.replaceAll("\\?", String.valueOf(messageId)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
