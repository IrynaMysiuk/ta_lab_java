package com.epam.lab.mobileService.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static com.epam.lab.mobileService.common.Constants.PATH_TO_DB_PROPERTIES;

public class ConfigProperties {

    public String getDBUrl() {
        return getPathDBProperties("db_url");
    }

    public String getDBUserName() {
        return getPathDBProperties("db_username");
    }

    public String getDBPassword() {
        return getPathDBProperties("db_password");
    }

    public String getDBDriver() {
        return getPathDBProperties("db_driver");
    }

    private String getPathDBProperties(String propertyName) {
        Properties aProp = new Properties();
        try {
            aProp.load(new FileInputStream(PATH_TO_DB_PROPERTIES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return aProp.getProperty(propertyName);
    }
}
