package com.epam.lab.mobileService.utils;

import com.epam.lab.mobileService.business.ClientBO;
import com.epam.lab.mobileService.entities.Call;
import com.epam.lab.mobileService.entities.Message;
import com.epam.lab.mobileService.entities.MobileClient;
import com.epam.lab.mobileService.entities.Tariff;

import java.util.Scanner;

public class MenuUtils {

    public static void chooseOption(Scanner scanner,ClientBO clientBO) throws NoSuchFieldException {
        System.out.println("Please enter \n1. Sign in as Admin\n2. Sign in as standard user");
        int chooseOption = scanner.nextInt();
        if (chooseOption == 1) {
            chooseAdminOption(scanner,clientBO);
        } else {
            chooseClientOption(scanner,clientBO);
        }
    }

    private static void chooseAdminOption(Scanner scanner,ClientBO clientBO) throws NoSuchFieldException {
        System.out.println("1.Show all data\n2.Add data\n3.Delete data");
        switch (scanner.nextInt()) {
            case 1: {
                clientBO.showAllData();
                break;
            }
            case 2: {
                clientBO.addData(new MobileClient(), new Tariff(),
                        new Message(), new Call());
                break;
            }
            case 3: {
                clientBO.deleteAllData();
                break;
            }
        }
    }

    private static void chooseClientOption(Scanner scanner,ClientBO clientBO) {
        System.out.println("1.Show all SMS\n2.Add SMS\n3.Add call");
        switch (scanner.nextInt()) {
            case 1: {
                clientBO.showAllMessage(1);
                break;
            }
            case 2: {
                clientBO.addMessage("Hello!", 2);
                break;
            }
            case 3: {
                clientBO.addCall(1);
                break;
            }
        }
    }
}
