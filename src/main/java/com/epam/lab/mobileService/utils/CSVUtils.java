package com.epam.lab.mobileService.utils;

import com.epam.lab.logger.Application;
import com.epam.lab.mobileService.entities.Call;
import com.epam.lab.mobileService.entities.Message;
import com.epam.lab.mobileService.entities.MobileClient;
import com.epam.lab.mobileService.entities.Tariff;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.mobileService.common.Constants.*;

public class CSVUtils {
    private static Logger logger = LogManager.getLogger(Application.class);

    private List<MobileClient> mobileClients = new ArrayList<>();
    private List<Tariff> tariffs = new ArrayList<>();
    private List<Message> messages = new ArrayList<>();
    private List<Call> calls = new ArrayList<>();

    private static MobileClient createClient(String[] metadata) {
        return new MobileClient()
                .setId(Integer.parseInt(metadata[0]))
                .setLastName(metadata[1])
                .setFirstName(metadata[2])
                .setBirthDate(Date.valueOf(metadata[3]))
                .setPhoneNumber(Integer.parseInt(metadata[4]))
                .setIdTariff(Integer.parseInt(metadata[5]))
                .setMoney(Integer.parseInt(metadata[6]));
    }

    public List<MobileClient> readClientData() {
        try (BufferedReader br = new BufferedReader(new FileReader(CLIENT_CSV_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(DELIMITER);
                MobileClient mobileClient = createClient(data);
                mobileClients.add(mobileClient);
                logger.info("Created " + mobileClient.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mobileClients;
    }

    private static Tariff createTariff(String[] metadata) {
        return new Tariff()
                .setIdTariff(Integer.parseInt(metadata[0]))
                .setPrice(Integer.parseInt(metadata[1]))
                .setInternet(Boolean.parseBoolean(metadata[2]))
                .setCalls(Boolean.parseBoolean(metadata[3]))
                .setMessage(Boolean.parseBoolean(metadata[4]));
    }

    public List<Tariff> readTariffData() {
        try (BufferedReader br = new BufferedReader(new FileReader(TARIFF_CSV_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(DELIMITER);
                Tariff tariff = createTariff(data);
                tariffs.add(tariff);
                logger.info("Created " + tariff.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tariffs;
    }

    private static Message createMessage(String[] metadata) {
        return new Message()
                .setIdMessage(Integer.parseInt(metadata[0]))
                .setIdSender(Integer.parseInt(metadata[1]))
                .setIdReceiver(Integer.parseInt(metadata[2]))
                .setTextMessage(metadata[3])
                .setTimeMessage(Time.valueOf(metadata[4]));
    }

    public List<Message> readMessageData() {
        try (BufferedReader br = new BufferedReader(new FileReader(SMS_CSV_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(DELIMITER);
                Message message = createMessage(data);
                messages.add(message);
                logger.info("Created " + message.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return messages;
    }

    private static Call createCall(String[] metadata) {
        return new Call()
                .setIdCall(Integer.parseInt(metadata[0]))
                .setIdSender(Integer.parseInt(metadata[1]))
                .setIdReceiver(Integer.parseInt(metadata[2]))
                .setDurationCall(Time.valueOf(metadata[3]));
    }

    public List<Call> readCallData() {
        try (BufferedReader br = new BufferedReader(new FileReader(CALL_CSV_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(DELIMITER);
                Call call = createCall(data);
                calls.add(call);
                logger.info("Created " + call.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return calls;
    }
}