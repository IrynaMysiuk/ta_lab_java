package com.epam.lab.mobileService.entities;

import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;

import java.sql.Time;

@DBTable("calls")
public class Call {

    @DB_Field("id_call")
    private int idCall;

    @DB_Field("id_receiver")
    private int idReceiver;

    @DB_Field("id_sender")
    private int idSender;

    @DB_Field("durationCall")
    private Time durationCall;

    public Call() {
    }

    public int getIdReceiver() {
        return idReceiver;
    }
//Use Builder Pattern
    public Call setIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
        return this;
    }

    public int getIdSender() {
        return idSender;
    }

    public Call setIdSender(int idSender) {
        this.idSender = idSender;
        return this;
    }

    public Time getDurationCall() {
        return durationCall;
    }

    public Call setDurationCall(Time durationCall) {
        this.durationCall = durationCall;
        return this;
    }

    public int getIdCall() {
        return idCall;
    }

    public Call setIdCall(int idCall) {
        this.idCall = idCall;
        return this;
    }

    @Override
    public String toString() {
        return "Call{" +
                "idCall=" + idCall +
                ", idReceiver=" + idReceiver +
                ", idSender=" + idSender +
                ", durationCall=" + durationCall +
                '}';
    }
}
