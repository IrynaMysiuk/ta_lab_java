package com.epam.lab.mobileService.entities;

import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;

import java.sql.Time;

@DBTable("messages")
public class Message {

    @DB_Field("id_message")
    private int idMessage;

    @DB_Field("id_receiver")
    private int idReceiver;

    @DB_Field("id_sender")
    private int idSender;

    @DB_Field("textMessage")
    private String textMessage;

    @DB_Field("timeMessage")
    private Time timeMessage;

    public Message() {
    }

    public int getIdReceiver() {
        return idReceiver;
    }

    public Message setIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
        return this;
    }

    public int getIdSender() {
        return idSender;
    }

    public Message setIdSender(int idSender) {
        this.idSender = idSender;
        return this;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public Message setTextMessage(String textMessage) {
        this.textMessage = textMessage;
        return this;
    }

    public Time getTimeMessage() {
        return timeMessage;
    }

    public Message setTimeMessage(Time timeMessage) {
        this.timeMessage = timeMessage;
        return this;
    }

    public int getIdMessage() {
        return idMessage;
    }

    public Message setIdMessage(int idMessage) {
        this.idMessage = idMessage;
        return this;
    }

    @Override
    public String toString() {
        return "Message{" +
                "idMessage=" + idMessage +
                ", idReceiver=" + idReceiver +
                ", idSender=" + idSender +
                ", textMessage='" + textMessage + '\'' +
                ", timeMessage=" + timeMessage +
                '}';
    }
}
