package com.epam.lab.mobileService.entities;

import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;

import java.sql.Date;

@DBTable("clients")
public class MobileClient {

    @DB_Field("id_client")
    private int idClient;

    @DB_Field("firstName")
    private String firstName;

    @DB_Field("lastName")
    private String lastName;

    @DB_Field("birthDate")
    private Date birthDate;

    @DB_Field("phoneNumber")
    private int phoneNumber;

    @DB_Field("id_tariff")
    private int idTariff;

    @DB_Field("money")
    private int money;

    public MobileClient() {
    }

    public int getId() {
        return idClient;
    }

    public MobileClient setId(int idClient) {
        this.idClient = idClient;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public MobileClient setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public MobileClient setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Date getCalls() {
        return birthDate;
    }

    public MobileClient setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public int getIdTariff() {
        return idTariff;
    }

    public MobileClient setIdTariff(int id_tariff) {
        this.idTariff = id_tariff;
        return this;
    }

    public int getMessage() {
        return phoneNumber;
    }

    public MobileClient setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public int getMoney() {
        return money;
    }

    public MobileClient setMoney(int money) {
        this.money = money;
        return this;
    }

    @Override
    public String toString() {
        return "MobileClient{" +
                "idClient=" + idClient +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", phoneNumber=" + phoneNumber +
                ", idTariff=" + idTariff +
                ", money=" + money +
                '}';
    }
}
