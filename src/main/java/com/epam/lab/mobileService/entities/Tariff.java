package com.epam.lab.mobileService.entities;

import com.epam.lab.mobileService.entities.annotations.DBTable;
import com.epam.lab.mobileService.entities.annotations.DB_Field;

@DBTable("tariffs")
public class Tariff {
    @DB_Field("id_tariff")
    private int idTariff;

    @DB_Field("price")
    private int price;

    @DB_Field("internet")
    private boolean internet;

    @DB_Field("calls")
    private boolean calls;

    @DB_Field("message")
    private boolean message;

    @DB_Field("id_clients")
    private int idClients;

    public Tariff() {
    }

    public int getId() {
        return idTariff;
    }

    public Tariff setId(int idTariff) {
        this.idTariff = idTariff;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Tariff setPrice(int price) {
        this.price = price;
        return this;
    }

    public boolean getInternet() {
        return internet;
    }

    public Tariff setInternet(boolean internet) {
        this.internet = internet;
        return this;
    }

    public boolean getCalls() {
        return calls;
    }

    public Tariff setCalls(boolean calls) {
        this.calls = calls;
        return this;
    }

    public int getIdTariff() {
        return idTariff;
    }

    public Tariff setIdTariff(int id_tariff) {
        this.idTariff = id_tariff;
        return this;
    }

    public boolean getMessage() {
        return message;
    }

    public Tariff setMessage(boolean message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "idTariff=" + idTariff +
                ", price=" + price +
                ", internet=" + internet +
                ", calls=" + calls +
                ", message=" + message +
                ", idClients=" + idClients +
                '}';
    }
}
