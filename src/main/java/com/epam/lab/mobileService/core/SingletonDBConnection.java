package com.epam.lab.mobileService.core;

import com.epam.lab.mobileService.utils.ConfigProperties;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingletonDBConnection {

    private static Connection connection;
//Use Singleton
    public static Connection getConnection() {
        ConfigProperties configProperties = new ConfigProperties();
        if (connection == null) {
            try {
                Class.forName(configProperties.getDBDriver());
                connection = DriverManager.getConnection
                        (configProperties.getDBUrl(), configProperties.getDBUserName(),
                                configProperties.getDBPassword());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
