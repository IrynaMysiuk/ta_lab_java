package com.epam.lab.mobileService.business;

import com.epam.lab.mobileService.daoImpl.CallDaoImpl;
import com.epam.lab.mobileService.daoImpl.ClientDaoImpl;
import com.epam.lab.mobileService.daoImpl.MessageDaoImpl;
import com.epam.lab.mobileService.daoImpl.TariffDaoImpl;
import com.epam.lab.mobileService.entities.Call;
import com.epam.lab.mobileService.entities.Message;
import com.epam.lab.mobileService.entities.MobileClient;
import com.epam.lab.mobileService.entities.Tariff;
import com.epam.lab.mobileService.utils.CSVUtils;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;

import static com.epam.lab.mobileService.common.Constants.MAX_RANDOM;
import static com.epam.lab.mobileService.common.Constants.RANDOM;

public class ClientBO {

    private ClientDaoImpl clientDao;
    private TariffDaoImpl tariffDao;
    private MessageDaoImpl messageDao;
    private CallDaoImpl callDao;
    private int currentUserId;

    public void preparationTables() {
        tariffDao = new TariffDaoImpl();
        tariffDao.createTariffTable();
        clientDao = new ClientDaoImpl();
        clientDao.createClientTable();
        messageDao = new MessageDaoImpl();
        messageDao.createMessageTable();
        callDao = new CallDaoImpl();
        callDao.createCallTable();
    }

    public void showAllData() {
        tariffDao.selectAll();
        clientDao.selectAll();
        messageDao.selectAll();
        callDao.selectAll();
    }
//Use Facad Pattern
    public void addData(MobileClient client, Tariff tariff, Message message, Call call) {
        tariffDao.insert(tariff);
        tariffDao.selectById(client.getId());
        clientDao.insert(client);
        clientDao.selectById(tariff.getId());
        messageDao.insert(message);
        messageDao.selectById(message.getIdMessage());
        callDao.insert(call);
        callDao.selectById(call.getIdCall());
    }

    public void loadData() {
        CSVUtils csvUtils = new CSVUtils();
        List<Tariff> tariffs = csvUtils.readTariffData();
        for (Tariff tariff : tariffs)
            tariffDao.insert(tariff);
        List<MobileClient> mobileClients = csvUtils.readClientData();
        for (MobileClient client : mobileClients)
            clientDao.insert(client);
        List<Message> messages = csvUtils.readMessageData();
        for (Message message : messages)
            messageDao.insert(message);
        List<Call> calls = csvUtils.readCallData();
        for (Call call : calls)
            callDao.insert(call);
    }

    public void deleteAllData() throws NoSuchFieldException {
        CSVUtils csvUtils = new CSVUtils();
        for (Message message : csvUtils.readMessageData())
            messageDao.deleteById(message.getIdMessage());
        for (Call call : csvUtils.readCallData())
            callDao.deleteById(call.getIdCall());
        for (MobileClient client : csvUtils.readClientData())
            clientDao.deleteById(client.getId());
        for (Tariff tariff : csvUtils.readTariffData())
            tariffDao.deleteById(tariff.getId());
    }

    public void showAllMessage(int id) {
        messageDao.selectById(id);
    }

    public void addMessage(String message, int idReceiver) {
        messageDao.insert(new Message()
                .setIdMessage(new Random().nextInt(RANDOM))
                .setTextMessage(message)
                .setTimeMessage(Time.valueOf(LocalTime.now()))
                .setIdReceiver(idReceiver)
                .setIdSender(currentUserId));
        MobileClient client = clientDao.selectById(currentUserId);
        Tariff tariff = tariffDao.selectById(client.getIdTariff());
        client.setMoney(client.getMoney() - tariff.getPrice());
        clientDao.update(client);
    }

    public void addCall(int idReceiver) {
        callDao.insert(new Call().
                setIdCall(new Random().nextInt(MAX_RANDOM))
                .setDurationCall(Time.valueOf(LocalTime.now()))
                .setIdReceiver(idReceiver)
                .setIdSender(currentUserId));
        MobileClient client = clientDao.selectById(currentUserId);
        Tariff tariff = tariffDao.selectById(client.getIdTariff());
        client.setMoney(client.getMoney() - tariff.getPrice());
        clientDao.update(client);
    }

    public void register(int typeNumber) {
        currentUserId = clientDao.selectByPhoneNumber(typeNumber).getId();
    }
}
