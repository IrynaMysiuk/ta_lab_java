package com.epam.lab.mobileService.common;

public class Constants {
    public static final String PATH_TO_DB_PROPERTIES = "src/main/resources/db.properties";
    public static final String TARIFF_CSV_FILE = "src/main/resources/mobile_service_data/tariffs.csv";
    public static final String CLIENT_CSV_FILE = "src/main/resources/mobile_service_data/clients.csv";
    public static final String SMS_CSV_FILE = "src/main/resources/mobile_service_data/SMS.csv";
    public static final String CALL_CSV_FILE = "src/main/resources/mobile_service_data/calls.csv";
    public static final String DELIMITER = ",";
    public static final int MAX_RANDOM = 1000;
    public static final int RANDOM = 100;

    private Constants() {
    }
}
