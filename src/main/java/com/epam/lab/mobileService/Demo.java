package com.epam.lab.mobileService;

import com.epam.lab.mobileService.business.ClientBO;

import java.util.Scanner;

import static com.epam.lab.mobileService.utils.MenuUtils.chooseOption;

public class Demo {

    public static void main(String[] args) throws NoSuchFieldException {
        ClientBO clientBO = new ClientBO();
        clientBO.preparationTables();
        clientBO.loadData();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your number: ");
        int typeNumber = scanner.nextInt();
        clientBO.register(typeNumber);
        chooseOption(scanner,clientBO);
    }
}
