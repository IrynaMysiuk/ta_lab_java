package com.epam.lab.enumMap;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

public class ToDoList {
    public TreeMap<String, String> toDoList = new TreeMap<>();
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public void addToDoList(String hour, String task) {
        toDoList.put(hour, task);
    }

    public void getToDoList(String hour) {
        logger.info(toDoList.get(hour));
    }

    public void removeToDoList(String hour) {
        toDoList.remove(hour);
    }

    public void printToDoList(String dayOfWeek) {
        for (Map.Entry toDo : toDoList.entrySet()) {
            logger.info(dayOfWeek + ": " + toDo.getKey() + " " + toDo.getValue());

        }
    }
}