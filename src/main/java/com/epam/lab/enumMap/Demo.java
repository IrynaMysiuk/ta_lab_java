package com.epam.lab.enumMap;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Demo {
    public static final int MAX_DAY_NUMBER = 8;
    public static final int MIN_DAY_NUMBER = 1;

    static ToDoList toDoList = new ToDoList();
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {
        logger.info("WELCOME IN YOUR PERSONAL PLANNER ON WEEK\n");
        for (int i = 0; i < 10; i++) {
            logger.info("Please choose day where you want input events" +
                    "\n1.MONDAY\n2.TUESDAY\n3.WEDNESDAY\n4.THURSDAY\n5.FRIDAY\n6.SATURDAY\n7.SUNDAY");
            Scanner scanner = new Scanner(System.in);
            int dayOfWeek = scanner.nextInt();
            if (dayOfWeek > MAX_DAY_NUMBER || dayOfWeek < MIN_DAY_NUMBER) {
                logger.error("You input incorrect numbers!");
            }
            chooseWeek(Week.values()[dayOfWeek - 1]);
        }
    }

    public static void chooseWeek(Week week) {
        switch (week) {
            case MONDAY: {
                logger.info("Your to-do list on Monday (" +
                        Week.MONDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.MONDAY.name());
                chooseOperation();
                break;
            }
            case TUESDAY: {
                logger.info("Your to-do list on Tuesday (" +
                        Week.TUESDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.TUESDAY.name());
                chooseOperation();
                break;
            }
            case WEDNESDAY: {
                logger.info("Your to-do list on Wednesday (" +
                        Week.WEDNESDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.WEDNESDAY.name());
                chooseOperation();
                break;
            }
            case THURSDAY: {
                logger.info("Your to-do list on Thursday (" +
                        Week.THURSDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.THURSDAY.name());
                chooseOperation();
                break;
            }
            case FRIDAY: {
                logger.info("Your to-do list on Friday (" +
                        Week.FRIDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.FRIDAY.name());
                chooseOperation();
                break;
            }
            case SATURDAY: {
                logger.info("Your to-do list on Saturday (" +
                        Week.SATURDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.SATURDAY.name());
                chooseOperation();
                break;
            }
            case SUNDAY: {
                logger.info("Your to-do list on Sunday (" +
                        Week.SUNDAY.getDayNumber() + ")");
                toDoList.printToDoList(Week.SUNDAY.name());
                chooseOperation();
                break;
            }
            default: {
                logger.info("What day is it?");
                break;
            }
        }
    }

    public static void chooseOperation() {
        logger.info("Please choose needed operation:\n1.Add event\n2.Show event by key\n3.Remove even by key ");
        Scanner scanner = new Scanner(System.in);
        int operation = scanner.nextInt();
        switch (operation) {
            case 1: {
                logger.info("Please input hour: ");
                String hour = scanner.next();
                logger.info("Please input task: ");
                String task = scanner.next();
                toDoList.addToDoList(hour, task);
                break;
            }
            case 2: {
                logger.info("Please input hour: ");
                String hour = scanner.next();
                toDoList.getToDoList(hour);
                break;
            }
            case 3: {
                logger.info("Please input hour: ");
                String hour = scanner.next();
                toDoList.removeToDoList(hour);
                break;
            }
        }
    }
}
