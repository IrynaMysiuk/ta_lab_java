package com.epam.lab.enumMap;

import com.epam.lab.logger.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class MyTreeMap<Key extends Comparable<Key>, Value> {
    static Logger logger = LogManager.getLogger(Application.class.getName());

    public static class Entry<K, V> {
        K key;
        V value;
        Entry right;
        Entry left;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public String toString() {
            return String.format("{%s=%s}", key, value);
        }
    }

    private Entry main = null;

    public Value getValue(Key key) {
        if (main == null) {
            return null;
        }
        Entry<Key, Value> cur = main;
        while (cur != null) {
            int r = key.compareTo(cur.key);
            if (r == 0) {
                return cur.value;
            } else if (r < 0) {
                cur = cur.left;
            } else {
                cur = cur.right;
            }
        }
        return null;
    }

    public Value put(Key key, Value value) {
        if (main == null) {
            main = new Entry<>(key, value);
            return null;
        }
        Entry<Key, Value> parent = null;
        Entry<Key, Value> cur = main;
        while (cur != null) {
            int r = key.compareTo(cur.key);
            if (r == 0) {
                Value oldValue = cur.value;
                cur.value = value;
                return oldValue;
            } else if (r < 0) {
                parent = cur;
                cur = cur.left;
            } else {
                parent = cur;
                cur = cur.right;
            }
        }
        Entry<Key, Value> entry = new Entry<>(key, value);
        if (key.compareTo(parent.key) < 0) {
            parent.left = entry;
        } else {
            parent.right = entry;
        }
        return null;
    }

    private void removeEntry(Entry<Key, Value> parent, Entry<Key, Value> cur) {
        if (cur.left == null) {
            if (cur == main) {
                main = main.right;
            } else if (cur == parent.right) {
                parent.right = cur.right;
            } else {
                parent.left = cur.right;
            }
        }
        if (cur.right == null) {
            if (cur == main) {
                main = main.left;
            } else if (cur == parent.left) {
                parent.left = cur.left;
            } else {
                parent.right = cur.left;
            }
        }
        if (cur.right != null && cur.left != null) {
            Entry<Key, Value> temp = cur.right;
            Entry<Key, Value> head = null;
            while (temp != null) {
                head = temp;
                temp = temp.left;
            }
            cur.key = temp.key;
            cur.value = temp.value;

            if (temp == head.left) {
                head.left = temp.right;
            } else if (temp == head.right) {
                head.right = temp.right;
            }
        }
    }

    public boolean remove(Key key) {
        if (main == null) {
            return false;
        }
        Entry<Key, Value> cur = main;
        Entry<Key, Value> parent = null;
        while (cur != null) {
            int r = key.compareTo(cur.key);
            if (r == 0) {
                removeEntry(parent, cur);
                return true;
            } else if (r < 0) {
                parent = cur;
                cur = cur.left;
            } else {
                parent = cur;
                cur = cur.right;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        MyTreeMap<Integer, String> tree = new MyTreeMap<>();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            int key = random.nextInt(200);
            String value = String.format("Value of %d", key);
            tree.put(key, value);
            logger.info("Key: " + key + " Value: " + tree.getValue(key));
        }
    }
}

